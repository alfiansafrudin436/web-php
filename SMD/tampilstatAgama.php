
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Dusun</th>
                      <th>Islam</th>
                      <th>Kristen</th>
                      <th>Katholik</th>
                      <th>Hindu</th>
                      <th>Buda</th>
                      <th>Konghuchu</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                    <th>Dusun</th>
                      <th>Islam</th>
                      <th>Kristen</th>
                      <th>Katholik</th>
                      <th>Hindu</th>
                      <th>Buda</th>
                      <th>Konghuchu</th>
                    </tr>
                  </tfoot>
                  <tbody>
            <?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, "SELECT dusun,
                count(IF (agama='Islam',1,NULL)) as A,
                count(IF (agama='Kristen',1,NULL)) as B,
                count(IF (agama='Katholik',1,NULL)) as C,
                count(IF (agama='Hindu',1,NULL)) as D,
                count(IF (agama='Budha',1,NULL)) as E,
                count(IF (agama='Konghuchu',1,NULL)) as F
                FROM penduduk GROUP by dusun");
                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
                <tr>
                    <td>
                        <?php echo $user_data['dusun']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['A']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['B']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['C']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['D']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['E']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['F']; ?>
                    </td>

                </tr>
        <?php
                }
        ?>
        </tbody>
    </table>

