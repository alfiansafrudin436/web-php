
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>Dusun</th>
                      <th>Belum/Tidak Bekerja</th>
                      <th>Mengurus Rumah Tangga</th>
                      <th>Pelajar/Mahasiswa</th>
                      <th>Pensiunan</th>
                      <th>PNS</th>
                      <th>TNI</th>
                      <th>Petani</th>
                      <th>Buruh</th>
                      <th>Wiraswasta</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Dusun</th>
                      <th>Belum/Tidak Bekerja</th>
                      <th>Mengurus Rumah Tangga</th>
                      <th>Pelajar/Mahasiswa</th>
                      <th>Pensiunan</th>
                      <th>PNS</th>
                      <th>TNI</th>
                      <th>Petani</th>
                      <th>Buruh</th>
                      <th>Wiraswasta</th>
                    </tr>
                  </tfoot>
                  <tbody>
            <?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, "SELECT dusun,
                count(IF (pekerjaan='Belum/Tidak Bekerja',1,NULL)) as A,
                count(IF (pekerjaan='Mengurus Rumah Tangga',1,NULL)) as B,
                count(IF (pekerjaan='Pelajar/Mahasiswa',1,NULL)) as C,
                count(IF (pekerjaan='Pensiunan',1,NULL)) as D,
                count(IF (pekerjaan='PNS',1,NULL)) as E,
                count(IF (pekerjaan='TNI',1,NULL)) as F,
                count(IF (pekerjaan='Petani',1,NULL)) as G,
                count(IF (pekerjaan='Buruh',1,NULL)) as H,
                count(IF (pekerjaan='Wiraswasta',1,NULL)) as I
                FROM penduduk GROUP by dusun");
                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
                <tr>
                    <td>
                        <?php echo $user_data['dusun']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['A']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['B']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['C']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['D']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['E']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['F']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['G']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['H']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['I']; ?>
                    </td>

                </tr>
        <?php
                }
        ?>
        </tbody>
    </table>

