<br>
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>No</th>
                      <th>Jenis Mutasi</th>
                      <th>NIK</th>
                      <th>Nama Pemohon</th>
                      <th>Aalasan Pindah</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Jenis Mutasi</th>
                      <th>NIK</th>
                      <th>Nama Pemohon</th>
                      <th>Aalasan Pindah</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
            <?php
                include"koneksi.php";
                $no = 1;
                $view = mysqli_query($mysqli, " select*
                                                  from
                                                  mutasi
                                                  order by namaPemohon ASC");

                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
        <tr>
            <td>
                <?php echo $no++; ?>
            </td>
            <td>
                <?php echo $user_data['jenisMutasi']; ?>
            </td>
            <td>
                <?php echo $user_data['nik']; ?>
            </td>
            <td>
                <?php echo $user_data['namaPemohon']; ?>
            </td>

            <td>
                <?php echo $user_data['alasanPindah']; ?>
            </td>
            <td>
            <center>

                <button title="Detail" type="button" class="view_data btn btn-primary btn-xs" id="btndetail" data-id=<?php echo $user_data['id']?> data-target="#myModal"><i class="fas fa-info-circle"></i></button>
                <button title="Ubah" type="button" class="btn btn-success btn-xs" id="btneditMutasi" data-id=<?php echo $user_data['id']?> ><i class="far fa-edit"></i></button>

            <center>
            </td>
        </tr>
        <?php
                }
        ?>
        </tbody>
    </table>

    <script src="js/demo/datatables-demo.js"></script>
