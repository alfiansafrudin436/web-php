$('#dtp').datetimepicker({
    format: 'MMM D, YYYY',
    widgetParent: 'body'
});

$('#dtp').on('dp.show', function () {
    var datepicker = $('body').find('.bootstrap-datetimepicker-widget:last');
    if (datepicker.hasClass('bottom')) {
        var top = $(this).offset().top + $(this).outerHeight();
        var left = $(this).offset().left;
        datepicker.css({
            'top': top + 'px',
            'bottom': 'auto',
            'left': left + 'px'
        });
    }
    else if (datepicker.hasClass('top')) {
        var top = $(this).offset().top - datepicker.outerHeight();
        var left = $(this).offset().left;
        datepicker.css({
            'top': top + 'px',
            'bottom': 'auto',
            'left': left + 'px'
        });
    }
});