$(document).ready(function() {
  var data = "tampilDataPendudukButton.php";
  $("#dataPenduduk").load(data);

  $("#form-tambah").submit(function(e) {
    e.preventDefault();

    $("#error_nik").html("");

    var dataform = $("#form-tambah").serialize();
    $.ajax({
      url: "inputPenduduk.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_nik").html(hasil.error.nik);
        } else {
          $("#add_data_Modal").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#nik").val("");
          $("#nama").val("");
          $("#noKK").val("");
          $("#alamat").val("");
          $("#dusun").val("");
          $("#rt").val("");
          $("#rw").val("");
          $("#rw").val("");
          $("#tempatLahir").val("");
          $("#kewarganegaraan").val("");
          $("#agama").val("");
          $("#pendidikan").val("");
          $("#pekerjaan").val("");
          $("#penghasilan").val("");
          $("#statusdalamKeluarga").val("");
          $("#statusPernikahan").val("");
          $("#dataPenduduk").load(data);
        }
      }
    });
  });
  $(document).on("click", "#btndetailPenduduk", function(e) {
    e.preventDefault();
    $("#modal-detail").modal("show");
    $.post("detailPenduduk.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-detail").html(html);
    });
  });

  $(document).on("click", "#btneditPenduduk", function(e) {
    e.preventDefault();
    $("#modal-edit").modal("show");
    $.post("editPendudukModal.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-edit").html(html);
    });
  });

  $("#form-edit").submit(function(e) {
    e.preventDefault();

    $("#error_edit_nik").html("");

    var dataform = $("#form-edit").serialize();
    $.ajax({
      url: "editPendudukQuery.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_edit_nik").html(hasil.error.edit_nik);
        } else {
          $("#modal-edit").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#nik").val("");
          $("#nama").val("");
          $("#noKK").val("");
          $("#alamat").val("");
          $("#dusun").val("");
          $("#rt").val("");
          $("#rw").val("");
          $("#rw").val("");
          $("#tempatLahir").val("");
          $("#kewarganegaraan").val("");
          $("#agama").val("");
          $("#pendidikan").val("");
          $("#pekerjaan").val("");
          $("#penghasilan").val("");
          $("#statusdalamKeluarga").val("");
          $("#statusPernikahan").val("");
          $("#dataPenduduk").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btnhapusPenduduk", function(e) {
    e.preventDefault();
    $("#modal-hapus").modal("show");
    $.post("hapusPendudukModal.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-hapus").html(html);
    });
  });
  $("#form-hapus").submit(function(e) {
    e.preventDefault();

    $("#error_hapus_id").html("");

    var dataform = $("#form-hapus").serialize();
    $.ajax({
      url: "hapusDataPenduduk.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_hapus_id").html(hasil.error.hapus_id);
        } else {
          $("#modal-hapus").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#dataPenduduk").load(data);
        }
      }
    });
  });
});
