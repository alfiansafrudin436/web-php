$(document).ready(function() {
  var data = "tampilDataMutasi.php";
  $("#dataMutasi").load(data);

  $(document).on("click", "#btndetail", function(e) {
    e.preventDefault();
    $("#modal-detail").modal("show");
    $.post("detailMutasi.php", { id: $(this).attr("data-id") }, function(html) {
      $("#data-detail").html(html);
    });
  });

  $("#form-tambah").submit(function(e) {
    e.preventDefault();
    $("#error_noMutasi").html("");

    var dataform = $("#form-tambah").serialize();
    $.ajax({
      url: "inputMutasi.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_noMutasi").html(hasil.error.noMutasi);
        } else {
          $("#add_data_Modal").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#noMutasi").val("");
          $("#jenisMutasi").val("");
          $("#kategoriPindah").val("");
          $("#nik").val("");
          $("#nomorKK").val("");
          $("#kepalaKeluarga").val("");
          $("#alamatAsal").val("");
          $("#rtAsal").val("");
          $("#rwAsal").val("");
          $("#provinsi").val("");
          $("#kabupaten").val("");
          $("#kecamatan").val("");
          $("#desaAsal").val("");
          $("#dusunAsal").val("");
          $("#kodeposAsal").val("");
          $("#telpAsal").val("");
          $("#namaPemohon").val("");
          $("#noKKTujuan").val("");
          $("#nikKKTujuan").val("");
          $("#alamatPindah").val("");
          $("#rtPindah").val("");
          $("#rwPindah").val("");
          $("#provinsiPindah").val("");
          $("#kabupatenPindah").val("");
          $("#kecamataNPindah").val("");
          $("#desaPindah").val("");
          $("#dusunPindah").val("");
          $("#kodeposTujuan").val("");
          $("#telpTujuan").val("");
          $("#jenisKepindahan").val("");
          $("#statusKKtdakPindah").val("");
          $("#statusKKPindah").val("");
          $("#alasanPindah").val("");
          $("#alasanMohon").val("");
          $("#dataMutasi").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btneditMutasi", function(e) {
    e.preventDefault();
    $("#modal-edit").modal("show");
    $.post("editMutasiModal.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-edit").html(html);
    });
  });

  $("#form-edit").submit(function(e) {
    e.preventDefault();
    $("#error_edit_noMutasi").html("");

    var dataform = $("#form-edit").serialize();
    $.ajax({
      url: "editMutasiQuery.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_edit_noMutasi").html(hasil.error.edit_noMutasi);
        } else {
          $("#modal-edit").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#noMutasi").val("");
          $("#nik").val("");
          $("#nomorKK").val("");
          $("#kepalaKeluarga").val("");
          $("#alamatAsal").val("");
          $("#rtAsal").val("");
          $("#rwAsal").val("");
          $("#provinsi").val("");
          $("#kabupaten").val("");
          $("#kecamatan").val("");
          $("#desaAsal").val("");
          $("#dusunAsal").val("");
          $("#kodeposAsal").val("");
          $("#telpAsal").val("");
          $("#namaPemohon").val("");
          $("#noKKTujuan").val("");
          $("#nikKKTujuan").val("");
          $("#alamatPindah").val("");
          $("#rtPindah").val("");
          $("#rwPindah").val("");
          $("#provinsiPindah").val("");
          $("#kabupatenPindah").val("");
          $("#kecamataPindah").val("");
          $("#desaPindah").val("");
          $("#dusunPindah").val("");
          $("#kodeposTujuan").val("");
          $("#telpTujuan").val("");
          $("#dataMutasi").load(data);
        }
      }
    });
  });
});
