$(document).ready(function() {
  var data = "tampilketMeninggal.php";
  $("#dataketMeninggal").load(data);

  $(document).on("click", "#btndetail", function(e) {
    e.preventDefault();
    $("#modal-detail").modal("show");
    $.post(
      "detailketMeninggal.php",
      { noSurat: $(this).attr("data-id") },
      function(html) {
        $("#data-detail").html(html);
      }
    );
  });

  $("#form-tambah").submit(function(e) {
    e.preventDefault();
    $("#error_noSurat").html("");

    var dataform = $("#form-tambah").serialize();
    $.ajax({
      url: "inputKetMeninggal.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_noSurat").html(hasil.error.noSurat);
        } else {
          $("#add_data_Modal").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#noSurat").val("");
          $("#nikMeninggal").val("");
          $("#nama").val("");
          $("#status").val("");
          $("#jenisKelamin").val("");
          $("#tempatLahir").val("");
          $("#tanggalLahir").val("");
          $("#wargaNegara").val("");
          $("#agama").val("");
          $("#pekerjaan").val("");
          $("#tempatTinggal").val("");
          $("#tanggalMeninggal").val("");
          $("#meninggalDi").val("");
          $("#pejabat").val("");
          $("#dataketMeninggal").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btneditketMeninggal", function(e) {
    e.preventDefault();
    $("#modal-edit").modal("show");
    $.post(
      "editketMeninggalModal.php",
      { noSurat: $(this).attr("data-id") },
      function(html) {
        $("#data-edit").html(html);
      }
    );
  });

  $("#form-edit").submit(function(e) {
    e.preventDefault();

    $("#error_noSurat").html("");

    var dataform = $("#form-edit").serialize();
    $.ajax({
      url: "editketMeninggalQuery.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_edit_noSurat").html(hasil.error.edit_no_Surat);
        } else {
          $("#modal-edit").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#noSurat").val("");
          $("#nikMeninggal").val("");
          $("#nama").val("");
          $("#status").val("");
          $("#jenisKelamin").val("");
          $("#tempatLahir").val("");
          $("#tanggalLahir").val("");
          $("#wargaNegara").val("");
          $("#agama").val("");
          $("#pekerjaan").val("");
          $("#tempatTinggal").val("");
          $("#tanggalMeninggal").val("");
          $("#meninggalDi").val("");
          $("#pejabat").val("");
          $("#dataketMeninggal").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btnhapusketMeninggal", function(e) {
    e.preventDefault();
    $("#modal-hapus").modal("show");
    $.post(
      "hapusketMeninggalModal.php",
      { noSurat: $(this).attr("data-id") },
      function(html) {
        $("#data-hapus").html(html);
      }
    );
  });
  $("#form-hapus").submit(function(e) {
    e.preventDefault();

    $("#error_hapus_noSurat").html("");

    var dataform = $("#form-hapus").serialize();
    $.ajax({
      url: "hapusdataketMeninggal.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_hapus_noSurat").html(hasil.error.hapus_noSurat);
        } else {
          $("#modal-hapus").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#dataketMeninggal").load(data);
        }
      }
    });
  });
});
