$(document).ready(function() {
  var data = "tampilketKelahiran.php";
  $("#dataketKelahiran").load(data);

  $(document).on("click", "#btndetail", function(e) {
    e.preventDefault();
    $("#modal-detail").modal("show");
    $.post(
      "detailketKelahiran.php",
      { noSurat: $(this).attr("data-id") },
      function(html) {
        $("#data-detail").html(html);
      }
    );
  });
  $("#form-tambah").submit(function(e) {
    e.preventDefault();
    $("#error_noSurat").html("");

    var dataform = $("#form-tambah").serialize();
    $.ajax({
      url: "inputKetKelahiran.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_noSurat").html(hasil.error.noSurat);
        } else {
          $("#add_data_Modal").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#noSurat").val("");
          $("#namaAnak").val("");
          $("#tempatLahir").val("");
          $("#anakKe").val("");
          $("#alamat").val("");
          $("#nikAyah").val("");
          $("#namaAyah").val("");
          $("#jenisKelaminAyah").val("");
          $("#ttlAyah").val("");
          $("#wargaNegaraAyah").val("");
          $("#agamaAyah").val("");
          $("#pekerjaanAyah").val("");
          $("#tempatTinggalAyah").val("");
          $("#nikIbu").val("");
          $("#namaIbu").val("");
          $("#jenisKelaminIbu").val("");
          $("#ttlIbu").val("");
          $("#wargaNegaraIbu").val("");
          $("#agamaIbu").val("");
          $("#pekerjaanIbu").val("");
          $("#tempatTinggalIbu").val("");
          $("#pejabat").val("");
          $("#dataketKelahiran").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btneditketKelahiran", function(e) {
    e.preventDefault();
    $("#modal-edit").modal("show");
    $.post(
      "editketKelahiranModal.php",
      { noSurat: $(this).attr("data-id") },
      function(html) {
        $("#data-edit").html(html);
      }
    );
  });

  $("#form-edit").submit(function(e) {
    e.preventDefault();

    $("#error_edit_noSurat").html("");
    $("#error_edit_namaAnak").html("");

    var dataform = $("#form-edit").serialize();
    $.ajax({
      url: "editketKelahiranQuery.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_edit_noSurat").html(hasil.error.edit_noSurat);
          $("#error_edit_namaAnak").html(hasil.error.edit_namaAnak);
        } else {
          $("#modal-edit").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#noSurat").val("");
          $("#namaAnak").val("");
          $("#tempatLahir").val("");
          $("#anakKe").val("");
          $("#alamat").val("");
          $("#nikAyah").val("");
          $("#namaAyah").val("");
          $("#jenisKelaminAyah").val("");
          $("#ttlAyah").val("");
          $("#wargaNegaraAyah").val("");
          $("#agamaAyah").val("");
          $("#pekerjaanAyah").val("");
          $("#tempatTinggalAyah").val("");
          $("#nikIbu").val("");
          $("#namaIbu").val("");
          $("#jenisKelaminIbu").val("");
          $("#ttlIbu").val("");
          $("#wargaNegaraIbu").val("");
          $("#agamaIbu").val("");
          $("#pekerjaanIbu").val("");
          $("#tempatTinggalIbu").val("");
          $("#pejabat").val("");
          $("#dataketKelahiran").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btnhapusketKelahiran", function(e) {
    e.preventDefault();
    $("#modal-hapus").modal("show");
    $.post(
      "hapusketKelahiranModal.php",
      { noSurat: $(this).attr("data-id") },
      function(html) {
        $("#data-hapus").html(html);
      }
    );
  });
  $("#form-hapus").submit(function(e) {
    e.preventDefault();

    $("#error_hapus_noSurat").html("");

    var dataform = $("#form-hapus").serialize();
    $.ajax({
      url: "hapusdataketKelahiran.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_hapus_noSurat").html(hasil.error.hapus_noSurat);
        } else {
          $("#modal-hapus").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#dataketKelahiran").load(data);
        }
      }
    });
  });
});
