$(document).ready(function() {
  var data = "tampilPermohonan.php";
  $("#dataPermohonan").load(data);

  $(document).on("click", "#btndetail", function(e) {
    e.preventDefault();
    $("#modal-detail").modal("show");
    $.post("detailPermohonan.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-detail").html(html);
    });
  });

  $(document).on("click", "#btnedit", function(e) {
    e.preventDefault();
    $("#modal-edit").modal("show");
    $.post("editPermohonanModal.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-edit").html(html);
    });
  });

  $("#form-edit").submit(function(e) {
    e.preventDefault();

    $("#error_edit_id").html("");

    var dataform = $("#form-edit").serialize();
    $.ajax({
      url: "editPermohonanQuery.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_edit_id").html(hasil.error.edit_nik);
        } else {
          $("#modal-edit").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#status").val("");
          $("#dataPermohonan").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btnhapus", function(e) {
    e.preventDefault();
    $("#modal-hapus").modal("show");
    $.post(
      "hapusPermohonanModal.php",
      { id: $(this).attr("data-id") },
      function(html) {
        $("#data-hapus").html(html);
      }
    );
  });
  $("#form-hapus").submit(function(e) {
    e.preventDefault();

    $("#error_hapus_id").html("");

    var dataform = $("#form-hapus").serialize();
    $.ajax({
      url: "hapusdataPermohonan.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_hapus_id").html(hasil.error.hapus_id);
        } else {
          $("#modal-hapus").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#dataPermohonan").load(data);
        }
      }
    });
  });
});
