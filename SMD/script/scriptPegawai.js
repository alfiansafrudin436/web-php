$(document).ready(function() {
  var data = "tampilPegawai.php";
  $("#dataPegawai").load(data);

  $(document).on("click", "#btndetail", function(e) {
    e.preventDefault();
    $("#modal-detail").modal("show");
    $.post("detailPegawai.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-detail").html(html);
    });
  });

  $("#form-tambah").submit(function(e) {
    e.preventDefault();
    $("#error_nik").html("");
    $("#error_nip").html("");
    $("#error_jabatan").html("");

    var dataform = $("#form-tambah").serialize();
    $.ajax({
      url: "inputPegawai.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_nik").html(hasil.error.nik);
          $("#error_nip").html(hasil.error.nip);
          $("#error_jabatan").html(hasil.error.jabatan);
        } else {
          $("#add_data_Modal").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#nik").val("");
          $("#nama").val("");
          $("#alamat").val("");
          $("#jabatan").val("");
          $("#nip").val("");
          $("#dataPegawai").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btnedit", function(e) {
    e.preventDefault();
    $("#modal-edit").modal("show");
    $.post("editPegawaiModal.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-edit").html(html);
    });
  });

  $("#form-edit").submit(function(e) {
    e.preventDefault();

    $("#error_edit_nik").html("");

    var dataform = $("#form-edit").serialize();
    $.ajax({
      url: "editPegawaiQuery.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_edit_nik").html(hasil.error.edit_nik);
        } else {
          $("#modal-edit").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#dataPegawai").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btnhapus", function(e) {
    e.preventDefault();
    $("#modal-hapus").modal("show");
    $.post("hapusPegawaiModal.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-hapus").html(html);
    });
  });
  $("#form-hapus").submit(function(e) {
    e.preventDefault();

    $("#error_hapus_id").html("");

    var dataform = $("#form-hapus").serialize();
    $.ajax({
      url: "hapusdataPegawai.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_hapus_id").html(hasil.error.hapus_id);
        } else {
          $("#modal-hapus").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#dataPegawai").load(data);
        }
      }
    });
  });
});
