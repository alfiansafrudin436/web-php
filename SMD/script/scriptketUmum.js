$(document).ready(function() {
  var data = "tampilketUmum.php";
  $("#dataketUmum").load(data);

  $(document).on("click", "#btndetail", function(e) {
    e.preventDefault();
    $("#modal-detail").modal("show");
    $.post("detailketUmum.php", { noSurat: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-detail").html(html);
    });
  });

  $("#form-tambah").submit(function(e) {
    e.preventDefault();
    $("#error_noSurat").html("");

    var dataform = $("#form-tambah").serialize();
    $.ajax({
      url: "inputketUmum.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_noSurat").html(hasil.error.noSurat);
        } else {
          $("#add_data_Modal").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#noSurat").val("");
          $("#nikPemohon").val("");
          $("#nama").val("");
          $("#status").val("");
          $("#jenisKelamin").val("");
          $("#tempatLahir").val("");
          $("#tanggalLahir").val("");
          $("#wargaNegara").val("");
          $("#agama").val("");
          $("#pekerjaan").val("");
          $("#tempatTinggal").val("");
          $("#tglSuratpengantar").val("");
          $("#noSuratpengantar").val("");
          $("#maksudSurat").val("");
          $("#pejabat").val("");
          $("#dataketUmum").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btneditketUmum", function(e) {
    e.preventDefault();
    $("#modal-edit").modal("show");
    $.post(
      "editketUmumModal.php",
      { noSurat: $(this).attr("data-id") },
      function(html) {
        $("#data-edit").html(html);
      }
    );
  });

  $("#form-edit").submit(function(e) {
    e.preventDefault();

    $("#error_edit_nama").html("");

    var dataform = $("#form-edit").serialize();
    $.ajax({
      url: "editketUmumQuery.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_edit_noSurat").html(hasil.error.edit_nama);
        } else {
          $("#modal-edit").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#noSurat").val("");
          $("#nikPemohon").val("");
          $("#nama").val("");
          $("#status").val("");
          $("#jenisKelamin").val("");
          $("#tempatLahir").val("");
          $("#tanggalLahir").val("");
          $("#wargaNegara").val("");
          $("#agama").val("");
          $("#pekerjaan").val("");
          $("#tempatTinggal").val("");
          $("#tglSuratpengantar").val("");
          $("#noSuratpengantar").val("");
          $("#maksudSurat").val("");
          $("#pejabat").val("");
          $("#dataketUmum").load(data);
        }
      }
    });
  });

  $(document).on("click", "#btnhapusketUmum", function(e) {
    e.preventDefault();
    $("#modal-hapus").modal("show");
    $.post(
      "hapusketUmumModal.php",
      { noSurat: $(this).attr("data-id") },
      function(html) {
        $("#data-hapus").html(html);
      }
    );
  });
  $("#form-hapus").submit(function(e) {
    e.preventDefault();

    $("#error_hapus_noSurat").html("");

    var dataform = $("#form-hapus").serialize();
    $.ajax({
      url: "hapusdataketUmum.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_hapus_noSurat").html(hasil.error.hapus_noSurat);
        } else {
          $("#modal-hapus").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#dataketUmum").load(data);
        }
      }
    });
  });
});
