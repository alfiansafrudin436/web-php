$(document).ready(function() {
  var data = "tampilDataPendudukMeninggal.php";
  $("#dataPenduduk").load(data);

  $(document).on("click", "#btndetail", function(e) {
    e.preventDefault();
    $("#modal-detail").modal("show");
    $.post(
      "detailPendudukMeninggal.php",
      { id: $(this).attr("data-id") },
      function(html) {
        $("#data-detail").html(html);
      }
    );
  });

  $(document).on("click", "#btneditPenduduk", function(e) {
    e.preventDefault();
    $("#modal-edit").modal("show");
    $.post("editPendudukModal.php", { id: $(this).attr("data-id") }, function(
      html
    ) {
      $("#data-edit").html(html);
    });
  });

  $("#form-edit").submit(function(e) {
    e.preventDefault();

    $("#error_nik").html("");

    var dataform = $("#form-edit").serialize();
    $.ajax({
      url: "editPendudukQuery.php",
      type: "post",
      data: dataform,
      success: function(result) {
        var hasil = JSON.parse(result);
        if (hasil.hasil !== "sukses") {
          $("#error_edit_nik").html(hasil.error.edit_nik);
        } else {
          $("#modal-edit").modal("hide");
          $("#modal_Sukses").modal("show");
          $("#nik").val("");
          $("#nama").val("");
          $("#noKK").val("");
          $("#alamat").val("");
          $("#dusun").val("");
          $("#rt").val("");
          $("#rw").val("");
          $("#tempatLahir").val("");
          $("#kewarganegaraan").val("");
          $("#agama").val("");
          $("#pendidikan").val("");
          $("#pekerjaan").val("");
          $("#penghasilan").val("");
          $("#statusdalamKeluarga").val("");
          $("#statusPernikahan").val("");
          $("#dataPenduduk").load(data);
        }
      }
    });
  });
});
