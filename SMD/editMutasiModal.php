
<?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, " select
                                                  id,
                                                  noMutasi,
                                                  jenisMutasi,
                                                  kategoriPindah,
                                                  nik,
                                                  nomorKK,
                                                  kepalaKeluarga,
                                                  alamatAsal,
                                                  rtAsal,
                                                  rwAsal,
                                                  provinsi,
                                                  kabupaten,
                                                  kecamatan,
                                                  desaAsal,
                                                  dusunAsal,
                                                  kodeposAsal,
                                                  telpAsal,
                                                  namaPemohon,
                                                  alasanPindah,
                                                  alasanMohon,
                                                  noKKTujuan,
                                                  nikKKTujuan,
                                                  tanggalKedatangan,
                                                  alamatPindah,
                                                  rtPindah,
                                                  rwPindah,
                                                  provinsiPindah,
                                                  kabupatenPindah,
                                                  kecamatanPindah,
                                                  desaPindah,
                                                  dusunPindah,
                                                  kodeposTujuan,
                                                  telpTujuan,
                                                  jenisKepindahan,
                                                  statusKKtdakPindah,
                                                  statusKKPindah
                                                  from
                                                  mutasi
                                                  where id= $_POST[id]");
                 $user_data = mysqli_fetch_array($view);

?>

            <form role="form" id="form-edit" method="post" action="editMutasiQuery.php">

                      <div class="form-group">
                      <input type="hidden" name="id" value="<?php echo $user_data['id'] ; ?>">
                      </div>


                      <div class="form-group">
                      <label class="font-weight-bold">No Surat</label>
                      <input type="text" name="noMutasi" class="form-control" value="<?php echo $user_data['noMutasi']; ?>" required/>
                      <p style="color:red" id="error_edit_noSurat"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Jenis Mutasi<sup class="text-danger">*</sup></label>
                      <select name="jenisMutasi" class="form-control" required>
                        <option value="Pindah Datang"<?php if ($user_data['jenisMutasi']=='Pindah Datang') {
    ; ?> selected <?php
}?>>Pindah Datang</option>
                        <option value="Pindah Keluar" <?php if ($user_data['jenisMutasi']=='Pindah Keluar') {
        ; ?> selected <?php
    }?>>Pindah Keluar</option>
                      </select>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Kategori Kepindahan</label>
                      <select name="kategoriPindah" class="form-control" required>
                        <option value="Antar Desa/ Kelurahan dalam Satu Kota" <?php if ($user_data['kategoriPindah']=='Antar Desa/ Kelurahan dalam Satu Kota') {
        ; ?> selected <?php
    }?>>Antar Desa/ Kelurahan dalam Satu Kota</option>
                        <option value="Antar Desa/ Kelurahan dalam Satu Provinsi" <?php if ($user_data['kategoriPindah']=='Antar Desa/ Kelurahan dalam Satu Provinsi') {
        ; ?> selected <?php
    }?>>Antar Desa/ Kelurahan dalam Satu Provinsi</option>
                        <option value="Antar Kota/ Kabupaten/ Provinsi" <?php if ($user_data['kategoriPindah']=='Antar Kota/ Kabupaten/ Provinsi') {
        ; ?> selected <?php
    }?>>Antar Kota/ Kabupaten/ Provinsi</option>
                      </select>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">NIK Pemohon<sup class="text-danger">*</sup></label>
                      <input type="text" name="nik" class="form-control" value="<?php echo $user_data['nik']; ?>"readonly/>
                      <p style="color:red" id="error_edit_nik"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Nomor KK<sup class="text-danger">*</sup></label>
                      <input type="text" name="nomorKK" class="form-control" value="<?php echo $user_data['nomorKK']; ?>"required/>
                      <p style="color:red" id="error_edit_nomorKK"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Nama Kepala Keluarga<sup class="text-danger">*</sup></label>
                      <input type="text" name="kepalaKeluarga" class="form-control" value="<?php echo $user_data['kepalaKeluarga']; ?>"required/>
                      <p style="color:red" id="error_edit_kepalaKeluarga"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Alamat Asal<sup class="text-danger">*</sup></label>
                      <textarea name="alamatAsal" class="form-control" required><?php echo $user_data['alamatAsal']; ?></textarea>
                      <p style="color:red" id="error_edit_alamatAsal"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">RT Asal</label>
                      <input type="text" name="rtAsal" class="form-control" value="<?php echo $user_data['rtAsal']; ?>"reuired/>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">RW Asal</label>
                      <input type="text" name="rwAsal" class="form-control" value="<?php echo $user_data['rwAsal']; ?>"/>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Provinsi<sup class="text-danger">*</sup></label>
                      <input type="text" name="provinsi" class="form-control" value="<?php echo $user_data['provinsi']; ?>"required/>
                      <p style="color:red" id="error_edit_provinsi"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Kabupaten/Kota<sup class="text-danger">*</sup></label>
                      <input type="text" name="kabupaten" class="form-control" value="<?php echo $user_data['kabupaten']; ?>"required/>
                      <p style="color:red" id="error_edit_kabupaten"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Kecamatan<sup class="text-danger">*</sup></label>
                      <input type="text" name="kecamatan" class="form-control" value="<?php echo $user_data['kecamatan']; ?>"required/>
                      <p style="color:red" id="error_edit_kecamatan"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Desa Asal<sup class="text-danger">*</sup></label>
                      <input type="text" name="desaAsal" class="form-control" value="<?php echo $user_data['desaAsal']; ?>"required/>
                      <p style="color:red" id="error_edit_desaAsal"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Dusun Asal<sup class="text-danger">*</sup></label>
                      <input type="text" name="dusunAsal" class="form-control" value="<?php echo $user_data['dusunAsal']; ?>"required/>
                      <p style="color:red" id="error_edit_dusunAsal"></p>
                      </div>


                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Kodepos Asal</label>
                      <input type="text" name="kodeposAsal" class="form-control" value="<?php echo $user_data['kodeposAsal']; ?>"/>
                      <p style="color:red" id="error_edit_dusunAsal"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Telp Asal</label>
                      <input type="text" name="telpAsal" class="form-control" value="<?php echo $user_data['telpAsal']; ?>"/>
                      <p style="color:red" id="error_edit_telpAsal"></p>
                      </div>


                      <div class="form-group">
                      <label class="font-weight-bold">Nama Pemohon<sup class="text-danger">*</sup></label>
                      <input type="text" name="namaPemohon" class="form-control" value="<?php echo $user_data['namaPemohon']; ?>"required/>
                      <p style="color:red" id="error_edit_namaPemohon"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Alasan Pindah<sup class="text-danger">*</sup></label>
                      <select name="alasanPindah" class="form-control"required>
                        <option value="Pekerjaan" <?php if ($user_data['alasanPindah']=='Pekerjaan') {
        ; ?> selected <?php
    }?>>Pekerjaan</option>
                        <option value="Pendidikan" <?php if ($user_data['alasanPindah']=='Pendidikan') {
        ; ?> selected <?php
    }?>>Pendidikan</option>
                        <option value="Keluarga" <?php if ($user_data['alasanPindah']=='Keluarga') {
        ; ?> selected <?php
    }?>>Keluarga</option>
                        <option value="Lainnya" <?php if ($user_data['alasanPindah']=='Lainnya') {
        ; ?> selected <?php
    }?>>Lainnya</option>
                      </select>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Alasan Permohonan<sup class="text-danger">*</sup></label>
                      <select name="alasanMohon" class="form-control"required>
                        <option value="Membentuk Rumah Tangga Baru" <?php if ($user_data['alasanMohon']=='Membentuk Rumah Tangga Baru') {
        ; ?> selected <?php
    }?>>Membentuk Rumah Tangga Baru</option>
                        <option value="KK Hilang/Rusak" <?php if ($user_data['alasanMohon']=='KK Hilang/Rusak') {
        ; ?> selected <?php
    }?>>KK Hilang/Rusak</option>
                        <option value="Lainnya" <?php if ($user_data['alasanMohon']=='Lainnya') {
        ; ?> selected <?php
    }?>>Lainnya</option>
                      </select>
                      </div>
                      <br>

                      <div class="form-group">
                      <label class="font-weight-bold">Nomor KK Tujuan<sup class="text-danger">*</sup></label>
                      <input type="text" name="noKKTujuan" class="form-control" value="<?php echo $user_data['noKKTujuan']; ?>"required/>
                      <p style="color:red" id="error_edit_noKKTujuan"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">NIK Kepala Keluarga Tujuan<sup class="text-danger">*</sup></label>
                      <input type="text" name="nikKKTujuan" class="form-control" value="<?php echo $user_data['nikKKTujuan']; ?>"required/>
                      <p style="color:red" id="error_edit_nikKKTujuan"></p>
                      </div>


                      <div class="form-group">
                     <label class="font-weight-bold">Tanggal Datang/Pindah<sup class="text-danger">*</sup></label>
                     <input type="date" name="tanggalKedatangan" class="form-control" value="<?php echo $user_data['tanggalKedatangan']; ?>" required/>
                     <p style="color:red" id="error_edit_tanggalKedatangan"></p>
                     </div>


                      <div class="form-group">
                      <label class="font-weight-bold">Alamat Tujuan<sup class="text-danger">*</sup></label>
                      <textarea name="alamatPindah" class="form-control" required><?php echo $user_data['alamatPindah']; ?></textarea>
                      <p style="color:red" id="error_edit_alamatPindah"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">RT Tujuan</label>
                      <input type="text" name="rtPindah" class="form-control" value="<?php echo $user_data['rtPindah']; ?>" />
                      <p style="color:red" id="error_edit_rtPindah"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">RW Tujuan</label>
                      <input type="text" name="rwPindah" class="form-control" value="<?php echo $user_data['rwPindah']; ?>" />
                      <p style="color:red" id="error_edit_rwPindah"></p>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Provinsi Tujuan<sup class="text-danger">*</sup></label>
                      <input type="text" name="provinsiPindah" class="form-control" value="<?php echo $user_data['provinsiPindah']; ?>" required/>
                      <p style="color:red" id="error_edit_provinsiPindah"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Kabupaten/Kota Tujuan<sup class="text-danger">*</sup></label>
                      <input type="text" name="kabupatenPindah" class="form-control" value="<?php echo $user_data['kabupatenPindah']; ?>" required/>
                      <p style="color:red" id="error_edit_kabupatenPindah"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Kecamatan Tujuan<sup class="text-danger">*</sup></label>
                      <input type="text" name="kecamatanPindah" class="form-control" value="<?php echo $user_data['kecamatanPindah']; ?>" required/>
                      <p style="color:red" id="error_edit_kecamatanPindah"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Desa Tujuan<sup class="text-danger">*</sup></label>
                      <input type="text" name="desaPindah" class="form-control" value="<?php echo $user_data['desaPindah']; ?>"required/>
                      <p style="color:red" id="error_edit_desaPindah"></p>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Dusun Tujuan<sup class="text-danger">*</sup></label>
                      <input type="text" name="dusunPindah" class="form-control" value="<?php echo $user_data['dusunPindah']; ?>"required/>
                      <p style="color:red" id="error_edit_dusunPindah"></p>
                      </div>


                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Kodepos Tujuan</label>
                      <input type="text" name="kodeposTujuan" class="form-control" value="<?php echo $user_data['kodeposTujuan']; ?>"/>
                      </div>

                      </div>
                      <div class="form-group">
                      <label class="font-weight-bold">Telp Tujuan</label>
                      <input type="text" name="telpTujuan" class="form-control" value="<?php echo $user_data['telpTujuan']; ?>"/>
                      </div>

                      <div class="form-group">
                          <label class="font-weight-bold">Jenis Kepindahan<sup class="text-danger">*</sup> </label>
                          <select name="jenisKepindahan" id="jenisKepindahan" class="form-control"required>
                            <option value="Kepala Keluarga" <?php if ($user_data['jenisKepindahan']=='Kepala Keluarga') {
        ; ?> selected <?php
    }?>>Kepala Keluarga</option>
                            <option value="Kep. Keluarga dan Seluruh Angg. Keluarga" <?php if ($user_data['jenisKepindahan']=='Kep. Keluarga dan Seluruh Angg. Keluarga') {
        ; ?> selected <?php
    }?>>Kep. Keluarga dan Seluruh Angg. Keluarga</option>
                            <option value="Kep. Keluarga dan Sebagian Angg. Keluarga" <?php if ($user_data['jenisKepindahan']=='Kep. Keluarga dan Sebagian Angg. Keluarga') {
        ; ?> selected <?php
    }?>>Kep. Keluarga dan Sebagian Angg. Keluarga</option>
                            <option value="Anggota Keluarga" <?php if ($user_data['jenisKepindahan']=='Anggota Keluarga') {
        ; ?> selected <?php
    }?>>Anggota Keluarga</option>
                          </select>
                          </div>

                          <div class="form-group">
                          <label class="font-weight-bold">Status KK Tidak Pindah<sup class="text-danger">*</sup> </label>
                          <select name="statusKKtdakPindah" id="statusKKtdakPindah" class="form-control"required>
                            <option value="Numpang KK" <?php if ($user_data['statusKKtdakPindah']=='Numpang KK') {
        ; ?> selected <?php
    }?>>Numpang KK</option>
                            <option value="Membuat KK Baru" <?php if ($user_data['statusKKtdakPindah']=='Membuat KK Baru') {
        ; ?> selected <?php
    }?>>Membuat KK Baru</option>
                            <option value="Tidak Ada Anggota Keluarga yang Ditinggal" <?php if ($user_data['statusKKtdakPindah']=='Tidak Ada Anggota Keluarga yang Ditinggal') {
        ; ?> selected <?php
    }?>>Tidak Ada Anggota Keluarga yang Ditinggal</option>
                            <option value="No KK Tetap" <?php if ($user_data['statusKKtdakPindah']=='No KK Tetap') {
        ; ?> selected <?php
    }?>>No KK Tetap</option>
                          </select>
                          </div>

                          <div class="form-group">
                          <label class="font-weight-bold">Status KK Pindah<sup class="text-danger">*</sup></label>
                          <select name="statusKKPindah" id="statusKKPindah" class="form-control"required>
                            <option value="Numpang KK" <?php if ($user_data['statusKKPindah']=='Numpang KK') {
        ; ?> selected <?php
    }?>>Numpang KK</option>
                            <option value="Membuat KK Baru" <?php if ($user_data['statusKKPindah']=='Membuat KK Baru') {
        ; ?> selected <?php
    }?>>Membuat KK Baru</option>
                            <option value="Nama Kepala Keluarga dan Nomor KK Tetap" <?php if ($user_data['statusKKPindah']=='Nama Kepala Keluarga dan Nomor KK Tetap') {
        ; ?> selected <?php
    }?>>Nama Kepala Keluarga dan Nomor KK Tetap</option>
                          </select>
                          </div>
                          <br>


            </form>
