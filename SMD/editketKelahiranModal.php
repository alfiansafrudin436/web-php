
<?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, " select * from ketkelahiran where noSurat= '$_POST[noSurat]'");
                 $user_data = mysqli_fetch_array($view);
                 if (!$view) {
                     printf("Error: %s\n", mysqli_error($mysqli));
                     exit();
                 }

?>

<form role="form" id="form-edit" method="post" action="editketKelahiran.php">

<div class="form-group">
  <label class="font-weight-bold">No Surat<sup class="text-danger">*</sup></label>
  <input type="text" name="noSurat"  class="form-control" value="<?php echo $user_data['noSurat']; ?>" required/>
  <p style="color:red" id="error_edit_noSurat"></p>
</div>

<div class="form-group">
  <label class="font-weight-bold">Nama Anak<sup class="text-danger">*</sup></label>
  <input type="text" name="namaAnak"  class="form-control" value="<?php echo $user_data['namaAnak']; ?>"required/>

</div>

<div class="form-group">
  <label class="font-weight-bold">Jenis Kelamin<sup class="text-danger">*</sup></label>
  <select name="jenisKelamin" class="form-control" required>
                        <option value="Laki-Laki" <?php if ($user_data['jenisKelamin']=='Laki-Laki') {
    ; ?> selected <?php
}?>>Laki-Laki</option>
                        <option value="Perempuan" <?php if ($user_data['jenisKelamin']=='Perempuan') {
        ; ?> selected <?php
    }?>>Perempuan</option>
                      </select>
</div>


<div class="form-group">
  <label class="font-weight-bold">Tempat Lahir<sup class="text-danger">*</sup></label>
  <textarea name="tempatLahir" class="form-control" required><?php echo $user_data['tempatLahir']; ?></textarea>
</div>

<br />

<div class="form-group">
  <label class="font-weight-bold">Tanggal Lahir<sup class="text-danger">*</sup></label>
  <input type="date" name="tanggalLahir" class="form-control" value="<?php echo $user_data['tanggalLahir']; ?>" required/>
</div>


<div class="form-group">
  <label class="font-weight-bold">Agama<sup class="text-danger">*</sup></label>
  <select name="agama" id="agama" class="form-control" required>
  <option value="Islam" <?php if ($user_data['agama']=='Islam') {
        ; ?> selected <?php
    }?> >Islam</option>
                        <option value="Kristen" <?php if ($user_data['agama']=='Kristen') {
        ; ?> selected <?php
    }?> >Kristen</option>
                        <option value="Katholik" <?php if ($user_data['agama']=='Khatolik') {
        ; ?> selected <?php
    }?> >Katholik</option>
                        <option value="Hindu" <?php if ($user_data['agama']=='Hindu') {
        ; ?> selected <?php
    }?> >Hindu</option>
                        <option value="Budha" <?php if ($user_data['agama']=='Budha') {
        ; ?> selected <?php
    }?> >Budha</option>
                        <option value="Khonghucu" <?php if ($user_data['agama']=='Khonghucu') {
        ; ?> selected <?php
    }?> >Khonghucu</option>
  </select>
</div>

<div class="form-group">
  <label class="font-weight-bold">Alamat<sup class="text-danger">*</sup></label>
  <textarea name="alamat" class="form-control" required><?php echo $user_data['alamat']; ?></textarea>

</div>

<div class="form-group">
  <label class="font-weight-bold">Anak Ke<sup class="text-danger">*</sup></label>
  <input type="text" name="anakKe" class="form-control" value="<?php echo $user_data['anakKe']; ?>"required/>

</div>


<div class="form-group">
<label class="font-weight-bold">NIK Ayah<sup class="text-danger">*</sup></label>
<input name="nikAyah"  class="form-control"  value="<?php echo $user_data['nikAyah']; ?>"required>
</div>

<div class="form-group">
  <label class="font-weight-bold">Nama Ayah</label>
  <input type="text" name="namaAyah"  class="form-control" value="<?php echo $user_data['namaAyah']; ?>"/>
</div>

<div class="form-group">
  <label class="font-weight-bold">Jenis Kelamin Ayah</label>
  <select name="jenisKelaminAyah" class="form-control" required>
                        <option value="Laki-Laki" <?php if ($user_data['jenisKelaminAyah']=='Laki-Laki') {
        ; ?> selected <?php
    }?>>Laki-Laki</option>
                        <option value="Perempuan" <?php if ($user_data['jenisKelaminAyah']=='Perempuan') {
        ; ?> selected <?php
    }?>>Perempuan</option>
                      </select>
</div>

<div class="form-group">
  <label class="font-weight-bold">Tempat Lahir Ayah</label>
  <input type="text" name="tempatLahirAyah" class="form-control" value="<?php echo $user_data['tempatLahirAyah']; ?>"/>
</div>
<div class="form-group">
  <label class="font-weight-bold">Tanggal Lahir Ayah</label>
  <input type="text" name="tanggalLahirAyah" class="form-control" value="<?php echo $user_data['tanggalLahirAyah']; ?>"/>
</div>

<div class="form-group">
  <label class="font-weight-bold">Kewarganegaraan Ayah</label>
  <input type="text" name="wargaNegaraAyah"  class="form-control" value="<?php echo $user_data['wargaNegaraAyah']; ?>"/>
</div>

<div class="form-group">
  <label class="font-weight-bold">Agama Ayah</label>
  <select name="agamaAyah" class="form-control" required>
                        <option value="Islam" <?php if ($user_data['agamaAyah']=='Islam') {
        ; ?> selected <?php
    }?> >Islam</option>
                        <option value="Kristen" <?php if ($user_data['agamaAyah']=='Kristen') {
        ; ?> selected <?php
    }?> >Kristen</option>
                        <option value="Katholik" <?php if ($user_data['agamaAyah']=='Khatolik') {
        ; ?> selected <?php
    }?> >Katholik</option>
                        <option value="Hindu" <?php if ($user_data['agamaAyah']=='Hindu') {
        ; ?> selected <?php
    }?> >Hindu</option>
                        <option value="Budha" <?php if ($user_data['agamaAyah']=='Budha') {
        ; ?> selected <?php
    }?> >Budha</option>
                        <option value="Khonghucu" <?php if ($user_data['agamaAyah']=='Khonghucu') {
        ; ?> selected <?php
    }?> >Khonghucu</option>
                      </select>
</div>

<div class="form-group">
  <label class="font-weight-bold">Pekerjaan Ayah</label>
  <select name="pekerjaanAyah" class="form-control" required>
                        <option value="Belum/Tidak Bekerja" <?php if ($user_data['pekerjaanAyah']=='Belum/Tidak Bekerja') {
        ; ?> selected <?php
    }?>>Belum/Tidak Bekerja</option>
                        <option value="Mengurus Rumah Tangga" <?php if ($user_data['pekerjaanAyah']=='Mengurus Rumah Tangga"') {
        ; ?> selected <?php
    }?>>Mengurus Rumah Tangga</option>
                        <option value="Pelajar/Mahasiswa" <?php if ($user_data['pekerjaanAyah']=='Pelajar/Mahasiswa') {
        ; ?> selected <?php
    }?>>Pelajar/Mahasiswa</option>
                        <option value="Pensiunan" <?php if ($user_data['pekerjaanAyah']=='Pensiunan') {
        ; ?> selected <?php
    }?>>Pensiunan</option>
                        <option value="PNS" <?php if ($user_data['pekerjaanAyah']=='PNS') {
        ; ?> selected <?php
    }?>>PNS</option>
                        <option value="TNI" <?php if ($user_data['pekerjaanAyah']=='TNI') {
        ; ?> selected <?php
    }?>>TNI</option>
                      </select>
</div>

<div class="form-group">
  <label class="font-weight-bold">Alamat Ayah<sup class="text-danger">*</sup></label>
  <textarea name="tempatTinggalAyah" class="form-control" ><?php echo $user_data['tempatTinggalAyah']; ?></textarea>
</div>

<div class="form-group">
<label class="font-weight-bold">NIK Ibu<sup class="text-danger">*</sup></label>
<input  type="text" name="nikIbu"  class="form-control" value="<?php echo $user_data['nikIbu']; ?>"required>
</div>

<div class="form-group">
  <label class="font-weight-bold">Nama Ibu</label>
  <input type="text" name="namaIbu" class="form-control" value="<?php echo $user_data['namaIbu']; ?>"/>
</div>

<div class="form-group">
  <label class="font-weight-bold">Jenis Kelamin Ibu</label>
  <select name="jenisKelaminIbu" class="form-control" required>
                        <option value="Laki-Laki" <?php if ($user_data['jenisKelaminIbu']=='Laki-Laki') {
        ; ?> selected <?php
    }?>>Laki-Laki</option>
                        <option value="Perempuan" <?php if ($user_data['jenisKelaminIbu']=='Perempuan') {
        ; ?> selected <?php
    }?>>Perempuan</option>
                      </select>
  </div>

<div class="form-group">
  <label class="font-weight-bold">Tanggal Lahir Ibu</label>
  <input type="date" name="tanggalLahirIbu"class="form-control"value="<?php echo $user_data['tanggalLahirIbu']; ?>" />
</div>
<div class="form-group">
  <label class="font-weight-bold">Tempat Lahir Ibu</label>
  <input type="text" name="tempatLahirIbu" class="form-control"value="<?php echo $user_data['tempatLahirIbu']; ?>" />
</div>

<div class="form-group">
  <label class="font-weight-bold">Kewarganegaraan Ibu</label>
  <input type="text" name="wargaNegaraIbu" class="form-control" value="<?php echo $user_data['wargaNegaraIbu']; ?>"/>
</div>

<div class="form-group">
  <label class="font-weight-bold">Agama Ibu</label>
  <select name="agamaIbu" class="form-control" required>
                        <option value="Islam" <?php if ($user_data['agamaIbu']=='Islam') {
        ; ?> selected <?php
    }?> >Islam</option>
                        <option value="Kristen" <?php if ($user_data['agamaIbu']=='Kristen') {
        ; ?> selected <?php
    }?> >Kristen</option>
                        <option value="Katholik" <?php if ($user_data['agamaIbu']=='Khatolik') {
        ; ?> selected <?php
    }?> >Katholik</option>
                        <option value="Hindu" <?php if ($user_data['agamaIbu']=='Hindu') {
        ; ?> selected <?php
    }?> >Hindu</option>
                        <option value="Budha" <?php if ($user_data['agamaIbu']=='Budha') {
        ; ?> selected <?php
    }?> >Budha</option>
                        <option value="Khonghucu" <?php if ($user_data['agamaIbu']=='Khonghucu') {
        ; ?> selected <?php
    }?> >Khonghucu</option>
                      </select>
</div>

<div class="form-group">
  <label class="font-weight-bold">Pekerjaan Ibu</label>
  <select name="pekerjaanIbu" class="form-control" required>
  <option value="Belum/Tidak Bekerja" <?php if ($user_data['pekerjaanIbu']=='Belum/Tidak Bekerja') {
        ; ?> selected <?php
    }?>>Belum/Tidak Bekerja</option>
                        <option value="Mengurus Rumah Tangga" <?php if ($user_data['pekerjaanIbu']=='Mengurus Rumah Tangga"') {
        ; ?> selected <?php
    }?>>Mengurus Rumah Tangga</option>
                        <option value="Pelajar/Mahasiswa" <?php if ($user_data['pekerjaanIbu']=='Pelajar/Mahasiswa') {
        ; ?> selected <?php
    }?>>Pelajar/Mahasiswa</option>
                        <option value="Pensiunan" <?php if ($user_data['pekerjaanIbu']=='Pensiunan') {
        ; ?> selected <?php
    }?>>Pensiunan</option>
                        <option value="PNS" <?php if ($user_data['pekerjaanIbu']=='PNS') {
        ; ?> selected <?php
    }?>>PNS</option>
                        <option value="TNI" <?php if ($user_data['pekerjaanIbu']=='TNI') {
        ; ?> selected <?php
    }?>>TNI</option>
                      </select>
</div>

<div class="form-group">
  <label class="font-weight-bold">Alamat Ibu</label>
  <textarea name="tempatTinggalIbu" class="form-control"><?php echo $user_data['tempatTinggalIbu']; ?></textarea>
</div>

<div class="form-group">
  <label class="font-weight-bold">Pejabat Yang Mengetahui<sup class="text-danger">*</sup></label>
  <input name="pejabat"  class="form-control"value="<?php echo $user_data['pejabat']; ?>" required>
</div>

</form>
