
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Dusun</th>
                      <th>Tidak/Belum Sekolah</th>
                      <th>Masih SD</th>
                      <th>SD</th>
                      <th>SMP</th>
                      <th>SMA</th>
                      <th>D3</th>
                      <th>S1</th>
                      <th>S2</th>
                      <th>S3</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Dusun</th>
                      <th>Tidak/Belum Sekolah</th>
                      <th>Masih SD</th>
                      <th>SD</th>
                      <th>SMP</th>
                      <th>SMA</th>
                      <th>D3</th>
                      <th>S1</th>
                      <th>S2</th>
                      <th>S3</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  
                  <?php
                include"koneksi.php";
                $no = 1;
                $view = mysqli_query($mysqli, "SELECT dusun,
                count(IF (pendidikan='Tidak/Belum Sekolah',1,NULL)) as A,
                count(IF (pendidikan='Belum Tamat SD/Sederajat',1,NULL)) as B,
                count(IF (pendidikan='SD/Sederajat',1,NULL)) as C,
                count(IF (pendidikan='SLTP/Sederajat',1,NULL)) as D,
                count(IF (pendidikan='SLTA/Sederajat',1,NULL)) as E,
                count(IF (pendidikan='D3',1,NULL)) as F,
                count(IF (pendidikan='S1',1,NULL)) as G,
                count(IF (pendidikan='S2',1,NULL)) as H,
                count(IF (pendidikan='S3',1,NULL)) as I
                FROM penduduk GROUP by dusun
                ");
                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
                <tr>
                    <td>
                        <?php echo $user_data['dusun']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['A']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['B']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['C']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['D']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['E']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['F']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['G']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['H']; ?>
                    </td>
                    <td>
                        <?php echo $user_data['I']; ?>
                    </td>
                </tr>
                <?php
                }
                ?>
        </tbody>
    </table>

