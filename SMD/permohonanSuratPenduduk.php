<?php


    session_start();

    if (!isset($_SESSION["usernamePenduduk"])) {
        echo "<script>alert('Anda harus login dulu');window.location='index.php'</script>";
        exit;
    }

    $username=$_SESSION["usernamePenduduk"];
    $nik=$_SESSION["nik"];
    $id=$_SESSION["id"];

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Permohonan Surat Penduduk</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/datepicker.css" />

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="permohonanSuratPenduduk.php">
      <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SMD<sup>Hargotirto</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="permohonanSuratPenduduk.php">
        <i class="fas fa-home"></i>
          <span>Halaman Utama</span></a>
      </li>

      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="daftarpermohonanSurat.php">
        <i class="fas fa-envelope"></i>
          <span>Daftar Permohonan</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

    </ul>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="rounded-circle border-0">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $username?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Keluar
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- End of Topbar -->


        <!-- Begin Page Content -->
        <div class="container-fluid col-sm-5"> 
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Form Permintaan Surat</h6>
            </div>
            <div class="card-body ">
            <form method="post" enctype="multipart/form-data" action="inputpermohonanSurat.php">

            <input type="hidden" name="id_penduduk" id="id_penduduk" value="<?php echo $id;?>" class="form-control" required/>

            <?php
                if (isset($_GET['pesan'])) {
                    $pesan = $_GET['pesan'];
                    if ($pesan == "input") {
                        ?>
                      <div class="alert alert-info text-center">
                      <?php
                      echo "Input Suskes, Silahkan Check Di Daftar Permohonan"; ?>
                      </div>
                      <?php
                    }
                    if ($pesan == "gagal") {
                        ?>
                      <div class="alert alert-info text-center">
                      <?php
                      echo "Input Gagal"; ?>
                      </div>
                      <?php
                    }
                    if ($pesan == "ukuran") {
                        ?>
                      <div class="alert alert-info text-center">
                      <?php
                      echo "Ukuran Gambar Terlalu Besar"; ?>
                      </div>
                      <?php
                    }
                    if ($pesan == "kesalahan") {
                        ?>
                      <div class="alert alert-info text-center">
                      <?php
                      echo "Terjadi Kesalahan input"; ?>
                      </div>
                      <?php
                    }
                    if ($pesan == "tipe") {
                        ?>
                      <div class="alert alert-info text-center">
                      <?php
                      echo "Pastikan file berformat JPG/PNG"; ?>
                      </div>
                      <?php
                    }
                }
            ?>
              <div class="form-group">
                <label class="font-weight-bold">NIK</label>
                <input type="text" name="nik" id="nik" class="form-control" value="<?php echo $nik ?>" readonly/>
              </div>

              <div class="form-group">
                <label class="font-weight-bold">Nama</label>
                <input type="text" name="nama" id="nama" class="form-control" value="<?php echo $username ?>" readonly/>
              </div>


            <?php
                include"koneksi.php";
            ?>

            <div class="form-group">
            <label class="font-weight-bold">Jenis Surat<sup class="text-danger">*</sup></label>
            <select id="jenisSurat" name="jenisSurat" class="form-control" style="width:100%" onchange='changeValue(this.value)' required> >
            <option value=""></option>
                        <?php
                        // ambil data dari database
                        $query = "SELECT * FROM jenissurat ORDER BY idsurat";
                        $hasil = mysqli_query($mysqli, $query);
                        $jsArray = "var prdName = new Array();\n";
                        while ($row = mysqli_fetch_array($hasil)) {
                            ?>
                            <option value="<?php echo $row['jenissurat'] ?>"><?php echo $row['jenissurat'] ?></option>
                            <?php
                            $jsArray .= "prdName['" . $row['jenissurat'] . "'] = {
                            keterangan:'".addslashes($row['keterangan'])."'};\n";
                        }
                        ?>
            </select>
            </div>
            <div class="form-group">
              <label class="font-weight-bold">Keterangan<sup class="text-danger">*</sup></label>
              <textarea type="text" name="keterangan" id="keterangan" class="form-control"></textarea>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Pilih Gambar<sup class="text-danger">*</sup></label>
              <input type="file" name="gambar" id="gambar">
            </div>

            <div class="form-group">
              <div class="radio">
                <input type="hidden" name="status" id="status" value="Belum Diproses" checked></input>
              </div>
            </div>

            <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit" value="submit">Minta Surat</button>
            </form>
            </div>
          </div>

        </div>

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Alfian Safrudin(Vael) 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin untuk keluar?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih "Keluar" jika anda ingin mengakhiri sesi anda.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" href="logout.php">Keluar</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="script/scriptketUmum.js"></script>

  <script src="js/js/datepicker.js"></script>
  <script>
      $(function() {
        $('[data-toggle="datepicker"]').datepicker({
          autoHide: true,
          zIndex: 2048
        });
      });
    </script>

</body>

</html>

<script type="text/javascript"> 
<?php echo $jsArray; ?>
function changeValue(id){
    document.getElementById('keterangan').value = prdName[id].keterangan;
};

</script>
