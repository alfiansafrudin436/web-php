<?php


    session_start();

    if (!isset($_SESSION["username"])) {
        echo "<script>alert('Anda harus login dulu');window.location='index.php'</script>";
        exit;
    }

    $username=$_SESSION["username"];
    ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Semua Penduduk</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/datepicker.css" />

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="Dashboard.php">
      <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SMD<sup>Hargotirto</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="Dashboard.php">
        <i class="fas fa-home"></i>
          <span>Halaman Utama</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Menu
      </div>

      <!-- Nav Item - Data Penunjang -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePenunjang" aria-expanded="true" aria-controls="collapsePenunjang">
        <i class="fas fa-server"></i>
          <span>Data Penunjang</span>
        </a>
        <div id="collapsePenunjang" class="collapse" aria-labelledby="headingPenunjang" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data Penunjang</h6>
            <a class="collapse-item" href="profilDesa.php">Profil Desa</a>
            <a class="collapse-item" href="visiMisi.php">Visi Misi</a>
            <a class="collapse-item" href="Pegawai.php">Pegawai</a>
          </div>
        </div>
      </li>

       <!-- Nav Item - Pages Collapse Menu -->
       <li class="nav-item active">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseKependudukan" aria-expanded="true" aria-controls="collapseKependudukan">
          <i class="fas fa-database"></i>
          <span>Kependudukan</span>
        </a>
        <div id="collapseKependudukan" class="collapse show" aria-labelledby="headingKependudukan" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Kependudukan</h6>
            <a class="collapse-item active" href="semuaPenduduk.php">Semua Penduduk</a>
            <a class="collapse-item " href="pendudukKK.php">Berdasarkan KK</a>
            <a class="collapse-item" href="pendudukMeninggal.php">Penduduk Meninggal</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseStatistik" aria-expanded="true" aria-controls="collapseStatistik">
        <i class="fas fa-database"></i>
          <span>Statistik</span>
        </a>
        <div id="collapseStatistik" class="collapse" aria-labelledby="headingStatistik" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Statistik</h6>
            <a class="collapse-item" href="statPendidikan.php">Pendidikan</a>
            <a class="collapse-item" href="statPekerjaan.php">Pekerjaan</a>
            <a class="collapse-item" href="statAgama.php">Agama</a>
            <a class="collapse-item" href="statJK.php">Jenis Kelamin</a>
          </div>
        </div>
      </li>
      
       <!-- Nav Item - Permintaan Surat -->
       <li class="nav-item">
        <a class="nav-link" href="dataPermohonan.php">
        <i class="fas fa-envelope"></i>
          <span>Permintaan Surat</span></a>
      </li>

      <!-- Nav Item - Layanan Surat -->
      <li class="nav-item ">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSurat" aria-expanded="true" aria-controls="collapseSurat">
        <i class="fas fa-envelope-open-text"></i>
          <span>Layanan Surat</span>
        </a>
        <div id="collapseSurat" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Layanan Surat</h6>
            <a class="collapse-item" href="ketMeninggal.php">Ket Meninggal</a>
            <a class="collapse-item " href="ketUmum.php">Ket Umum</a>
            <a class="collapse-item" href="ketKelahiran.php">Ket Kelahiran</a>
          </div>
        </div>
      </li>


      <!-- Nav Item - Mutasi -->
      <li class="nav-item">
        <a class="nav-link" href="dataMutasi.php">
        <i class="fas fa-fw fa-plane"></i>
          <span>Mutasi</span></a>
      </li>



      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

    </ul>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="rounded-circle border-0">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $username?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Keluar
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- End of Topbar -->


        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Semua Penduduk</h1>
          <p class="mb-4">Menampilkan Data Semua Penduduk</a>.</p>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Semua Penduduk</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <div align="left">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_data_Modal" class="btn btn-primary">Tambah Penduduk</button>
                </div>
                <!--TEMPAT TABEL DIPANGGIL-->
                <div id="dataPenduduk">
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Alfian Safrudin(Vael) 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
<!-- MODAL HAPUS -->
<div id="modal-hapus" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form role="form" id="form-hapus" method="post">
          <div class="modal-header">
            <h4 class="modal-title">Hapus Data Penduduk</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div id="data-hapus"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Hapus</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <!-- MODAL UNTUK INSERT -->
  <div id="add_data_Modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title-bold">Tambah Data Penduduk</h4>
          <div align="right">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        </div>
        <div class="modal-body">
          <form role="form" id="form-tambah" method="post" action="inputPenduduk.php">

            <div class="form-group">
              <label class="font-weight-bold">NIK<sup class="text-danger">*</sup></label>
              <input type="text" name="nik" id="nik" class="form-control" required/>
              <p style="color:red" id="error_nik"></p>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Nama<sup class="text-danger">*</sup></label>
              <input type="text" name="nama" id="nama" class="form-control" required/>
            </div>
              <input type="hidden" name="username" id="username" value="<?php echo $username;?>" class="form-control" required/>

            <div class="form-group">
              <label class="font-weight-bold">Nomor KK<sup class="text-danger">*</sup></label>
              <input type="text" name="noKK" id="noKK" class="form-control" required/>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Alamat<sup class="text-danger">*</sup></label>
              <textarea name="alamat" id="alamat" class="form-control"required></textarea>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Dusun<sup class="text-danger">*</sup></label>
              <select name="dusun" id="dusun" class="form-control" required>
                <option value="">--Pilih--</option>
                <option value="Soropati">Soropati</option>
                <option value="Sekendal">Sekendal</option>
                <option value="Segajih">Segajih</option>
                <option value="Keji">Keji</option>
                <option value="Teganing I">Teganing I</option>
                <option value="Teganing II">Teganing II</option>
                <option value="Teganing III">Teganing III</option>
                <option value="Tirto">Tirto</option>
                <option value="Crangah">Crangah</option>
                <option value="Sungapan I">Sungapan I</option>
                <option value="Sungapan II">Sungapan II</option>
                <option value="Menguri">Menguri</option>
                <option value="Sebatang">Sebatang</option>
                <option value="Nganti">Nganti</option>
              </select>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">RT<sup class="text-danger">*</sup></label>
              <input type="text" name="rt" id="rt" class="form-control" required/>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">RW<sup class="text-danger">*</sup></label>
              <input type="text" name="rw" id="rw" class="form-control" required/>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Jenis Kelamin<sup class="text-danger">*</sup></label>
              <select name="jenisKelamin" id="jenisKelamin" class="form-control" required>
                <option value="">--Pilih--</option>
                <option value="Laki-Laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Tempat Lahir<sup class="text-danger">*</sup></label>
              <input type="text" name="tempatLahir" id="tempatLahir" class="form-control" required/>
            </div>

            <br />


            <div class="form-group">
              <label class="font-weight-bold">Tanggal Lahir<sup class="text-danger">*</sup></label>
              <input type="text" name="tanggalLahir" id="tanggalLahir" class="form-control" data-toggle="datepicker" />
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Kewarganegaraan<sup class="text-danger">*</sup></label>
              <input type="text" name="kewarganegaraan" id="kewarganegaraan" class="form-control" required/>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Agama<sup class="text-danger">*</sup></label>
              <select name="agama" id="agama" class="form-control" required>
                <option value="">--Pilih--</option>
                <option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Katholik">Katholik</option>
                <option value="Hindu">Hindu</option>
                <option value="Budha">Budha</option>
                <option value="Khonghucu">Khonghucu</option>
              </select>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Pendidikan<sup class="text-danger">*</sup></label>
              <select name="pendidikan" id="pendidikan" class="form-control" required>
               <option value="">--Pilih--</option>
                <option value="Tidak/Belum Sekolah">Tidak/Belum Sekolah</option>
                <option value="Belum Tamat SD/Sederajat">Belum Tamat SD/Sederajat</option>
                <option value="SD/Sederajat">SD/Sederajat</option>
                <option value="SLTP/Sederajat">SLTP/Sederajat</option>
                <option value="SLTA/Sederajat">SLTA/Sederajat</option>
                <option value="D3">D3</option>
                <option value="S1">S1</option>
                <option value="S2">S2</option>
                <option value="S3">S3</option>
              </select>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Pekerjaan<sup class="text-danger">*</sup></label>
              <select name="pekerjaan" id="pekerjaan" class="form-control" required>
               <option value="">--Pilih--</option>
                <option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja</option>
                <option value="Mengurus Rumah Tangga">Mengurus Rumah Tangga</option>
                <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                <option value="Pensiunan">Pensiunan</option>
                <option value="PNS">PNS</option>
                <option value="TNI">TNI</option>
                <option value="Petani">Petani</option>
                <option value="Buruh">Buruh</option>
                <option value="Wiraswasta">Wiraswasta</option>
              </select>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Pengahasilan</label>
              <input type="text" name="penghasilan" id="penghasilan" class="form-control"/>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Status dalam Keluarga<sup class="text-danger">*</sup></label>
              <select name="statusdalamKeluarga" id="statusdalamKeluarga" class="form-control" required>
              <option value="">--Pilih--</option>
                <option value="Kepala Keluarga">Kepala Keluarga</option>
                <option value="Suami">Suami</option>
                <option value="Istri">Istri</option>
                <option value="Anak">Anak</option>
                <option value="Menantu">Menantu</option>
                <option value="Cucu">Cucu</option>
              </select>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Status Pernikahan<sup class="text-danger">*</sup></label>
              <select name="statusPernikahan" id="statusPernikahan" class="form-control" required>
              <option value="">--Pilih--</option>
                <option value="Belum Kawin">Belum Kawin</option>
                <option value="Kawin">Kawin</option>
                <option value="Cerai Hidup">Cerai Hidup</option>
                <option value="Cerai Mati">Cerai Mati</option>
              </select>
            </div>

            <div class="form-group">
              <label class="font-weight-bold">Golongan Darah</label>
              <select name="golDarah" id="golDarah" class="form-control" required>
                <option value="-">-</option>
                <option value="A">A</option>
                <option value="AB">AB</option>
                <option value="B">B</option>
                <option value="O">O</option>
              </select>
            </div>

            <div class="form-group">
              <div class="radio">
                <input type="hidden" name="status" value="Hidup" checked></input>
              </div>
            </div>
            

            <br>
        <div class="modal-footer">
        <button type="submit" name="submit" id="submit" class="btn btn-success">Tambah Data</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>

  <div id="modal-edit" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form role="form" id="form-edit" method="post" action="inputPenduduk.php">
          <div class="modal-header">
            <h4 class="modal-title">Ubah Data Penduduk</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div id="data-edit"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!--MODAL SUKSES-->
  <div class="modal fade" id="modal_Sukses" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="form-sukses">SUKSES</h5>
        </div>
        <form>
          <center>
            <i class="far fa-check-circle fa-5x "></i>
          </center>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>

  <!--MODAL DETAIL-->
  <div id="modal-detail" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form role="form" id="form-detail" method="post" action="inputPenduduk.php">
          <div class="modal-header">
            <h4 class="modal-title">Detail Data Penduduk</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div id="data-detail"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin untuk keluar?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih "Keluar" jika anda ingin mengakhiri sesi anda.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" href="logout.php">Keluar</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="script/scriptusingButton.js"></script>

  <script src="js/js/datepicker.js"></script>
  <script>
      $(function() {
        $('[data-toggle="datepicker"]').datepicker({
          autoHide: true,
          zIndex: 2048
        });
      });
    </script>

</body>

</html>

</script>
