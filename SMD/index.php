
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Alfian">
  <meta name="author" content="Alfian">

  <title>SMD LOGIN</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body>
      <div class="card-body ">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row no-gutter">
            <div class="col-md-9 col-lg-5 mx-auto">
              <center>
              <h3 class="login-heading mb-4">SELAMAT DATANG</h3>
              </center>
              <form method="post" action="login.php">
                <div class="form-label-group">
                  <input type="text" name="username" class="form-control" placeholder="NIK Bagi Penduduk" required autofocus>
                  <label>Username</label>
                </div>

                <div class="form-label-group">
                  <input type="password" name="password" class="form-control" placeholder="NIK Bagi Penduduk" required>
                  <label>Password</label>
                </div>

                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit" value="Login">MASUK</button>
              </form>
           </div>
          </div>
        </div>
      </div>
    </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
