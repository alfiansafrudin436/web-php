
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>Alamat</th>
                      <th>JK</th>
                      <th>S.Perkawinan</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>Alamat</th>
                      <th>JK</th>
                      <th>S.Perkawinan</th>
                    </tr>
                  </tfoot>
                  <tbody>
            <?php
                include"koneksi.php";
                $no = 1;
                $view = mysqli_query($mysqli, " select
                                                        id,
                                                        nik,
                                                        nama,
                                                        alamat,
                                                        jenisKelamin,
                                                        statusPernikahan
                                                  from
                                                  penduduk
                                                  order by nama ASC");
                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
        <tr>
            <td>
                <?php echo $no++; ?>
            </td>
            <td>
                <?php echo $user_data['nik']; ?>
            </td>
            <td>
                <?php echo $user_data['nama']; ?>
            </td>
            <td>
                <?php echo $user_data['alamat']; ?>
            </td>
            <td>
                <?php echo $user_data['jenisKelamin']; ?>
            </td>
            <td>
                <?php echo $user_data['statusPernikahan']; ?>
            </td>
        </tr>
        <?php
                };
        ?>
        </tbody>
    </table>

    <script src="js/demo/datatables-demo.js"></script>
