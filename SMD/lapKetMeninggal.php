<?php
session_start();
include'koneksi.php';
require('pdf/fpdf.php');

$noSurat=$_GET['id'];
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}



//$noSurat = $_GET["noSurat"];

$result = mysqli_query($mysqli, "SELECT * FROM ketmeninggal WHERE noSurat = '$noSurat'");
    if (!$result) {
        printf("Error: %s\n", mysqli_error($mysqli));
        exit();
    }
$query = mysqli_fetch_assoc($result);
$tanggalMeninggal = $query['tanggalMeninggal'];
$tanggalMeninggal = tgl_indo(date('Y-m-d', strtotime($tanggalMeninggal)));
$hariMeninggal = tgl_indo(date('Y-m-d', strtotime($tanggalMeninggal)));
$hariIni=tgl_indo(date('Y-m-d'));

$birthDt = new DateTime($query['tanggalLahir']);
  //tanggal hari ini
  $today = new DateTime($query['tanggalMeninggal']);
  //tahun
  $y = $today->diff($birthDt)->y;
  //bulan
  $m = $today->diff($birthDt)->m;
  //hari
  $d = $today->diff($birthDt)->d;
  $birthDt = new DateTime($query['tanggalLahir']);
          //tanggal hari ini
          $today = new DateTime($query['tanggalMeninggal']);
          //tahun
          $y = $today->diff($birthDt)->y;
          //bulan
          $m = $today->diff($birthDt)->m;
          //hari
          $d = $today->diff($birthDt)->d;
         if ($y < 2) {
             if ($y!=0) {
                 $tahun = $y."tahun";
             } else {
                 $tahun = "";
             }
         }


$pdf = new FPDF("P", "cm", "A4");
$pdf->SetMargins(2, 1, 1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Image('img/logo.png', 1, 1, 3, 3);
$pdf->SetFont('Times', 'B', 16);
$pdf->SetX(2);
$pdf->MultiCell(18, 0.7, 'PEMERINTAH KABUPATEN KULON PROGO', 0, 'C');
$pdf->MultiCell(18, 0.7, 'PANEWON KOKAP', 0, 'C');
$pdf->SetFont('Times', 'B', 20);
$pdf->SetX(2);
$pdf->MultiCell(18, 0.7, 'KALURAHAN HARGOTIRTO', 0, 'C');
$pdf->SetX(2);
$pdf->SetFont('Arial', 'B', 10);
$pdf->SetX(2);
$pdf->MultiCell(18, 0.5, 'Sekendal, Hargotirto, Kec. Kokap, Kabupaten Kulonprogo, DIY - Kodepos 55653', 0, 'C');
$pdf->SetX(2);
$pdf->MultiCell(18, 0.5, 'Telp : 08112635610   Email : desahargotirto@yahoo.co.id', 0, 'C');
$pdf->Line(1, 4.2, 20, 4.2);
$pdf->SetLineWidth(0.1);
$pdf->Line(1, 4.3, 20, 4.3);
$pdf->SetLineWidth(0);
$pdf->SetFont('Arial', 'B', 13);
$pdf->Cell(18, 2, "SURAT KETERANGAN MENINGGAL", 0, 10, 'C');
$pdf->Line(7, 5.5, 15, 5.5);
$pdf->SetLineWidth(0.1);
$pdf->Cell(15, 0, 'NOMOR :', 0, 0, 'C');
$pdf->SetX(2);
$pdf->Cell(19, 0, $query['noSurat'], 0, 0, 'C');
$pdf->SetX(2);
$pdf->SetFont('Arial', '', 12);
$pdf->ln(2);
$pdf->SetX(3);
$pdf->Cell(0, 1, "Yang bertanda tangan dibawah ini Kepala Desa Hargotirto Kec. Kokap Kabupaten ", 0, 3, 'L');
$pdf->SetX(2);
$pdf->Cell(0, 0, "Kulonprogo. Menerangkan dengan sebenarnya bahwa : ", 0, 3, 'L');
$pdf->ln(1);
$pdf->SetX(4);
$pdf->Cell(0, 0, 'Nama', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 0, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 0, $query['nama'], 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 1, 'Jenis Kelamin', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 1, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 1, $query['jenisKelamin'], 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 2, 'Umur', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 2, ':', 0, 0, 'L');

if ($y>=1) {
    $pdf->SetX(7);
    $pdf->Cell(0, 2, $y, 0, 0, 'L');
    $pdf->SetX(7.5);
    $pdf->Cell(0, 2, 'tahun', 0, 0, 'L');
    $pdf->SetX(8.8);
    $pdf->Cell(0, 2, $m, 0, 0, 'L');
    $pdf->SetX(9.3);
    $pdf->Cell(0, 2, 'bulan', 0, 0, 'L');
} else {
    $pdf->SetX(7);
    $pdf->Cell(0, 2, $m, 0, 0, 'L');
    $pdf->SetX(7.5);
    $pdf->Cell(0, 2, 'bulan', 0, 0, 'L');
}

$pdf->SetX(4);
$pdf->Cell(0, 3, 'Alamat', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 3, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 3, $query['tempatTinggal'], 0, 0, 'L');
$pdf->SetX(4);
$pdf->ln(2);
$pdf->Cell(0, 1, "Telah dinyatakan meninggal dunia pada : ", 0, 3, 'L');
$pdf->ln(0.5);
$pdf->SetX(4);
$pdf->Cell(0, 0, 'Hari', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 0, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 0, $d, 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 1, 'Tanggal', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 1, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 1, $tanggalMeninggal, 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 2, 'Tempat', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 2, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 2, $query['meninggalDi'], 0, 0, 'L');
$pdf->ln(2);
$pdf->SetX(3);
$pdf->Cell(0, 0, "Demikian surat keterangan kematian ini diberikan kepada yang bersangkutan ", 0, 3, 'L');
$pdf->SetX(2);
$pdf->Cell(0, 1, "untuk dapat dipergunakan sebagaimana mestinya. ", 0, 3, 'L');
$pdf->ln(5);
$pdf->SetX(13);
$pejabat=$query['pejabat'];
$q=mysqli_query($mysqli, "Select * from pegawai where nama ='$pejabat'");
$data=mysqli_fetch_assoc($q);
if ($data['jabatan']=='Lurah') {
    $pdf->Cell(0, 0, $tanggalMeninggal, 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $data['jabatan'], 0, 0, 'C');
    $pdf->ln(3);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $pejabat, 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $data['nip'], 0, 0, 'C');
} else {
    $pdf->Cell(0, 0, $tanggalMeninggal, 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, 'An. Lurah', 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $data['jabatan'], 0, 0, 'C');
    $pdf->ln(3);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $pejabat, 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $data['nip'], 0, 0, 'C');
}
$pdf->Output("ketMeninggal.pdf", "I");
