
<?php
//memasukkan koneksi database
require_once("koneksi.php");

//jika berhasil/ada post['id'], jika tnikak ada ya tidak dijalankan
if ($_POST['noSurat']) {
    //membuat variabel id berisi post['id']
    $noSurat = $_POST['noSurat'];
    //query standart select where id
    $view = $mysqli->query("SELECT * FROM ketumum WHERE noSurat='$noSurat'");
    //jika ada datanya
    if ($view->num_rows) {
        //fetch data ke dalam veriabel $user_data
        $user_data = $view->fetch_assoc();
        //menampilkan data dengan table
        echo '
		<p>Berikut ini adalah detail dari data surat keterangan umum dengan nama <b>' . $user_data['nama'] . '</b></p>
		<table class="table table-bordered">
			<tr>
				<th>NO SURAT</th>
				<td>' . $user_data['noSurat'] . '</td>
			</tr>
			<tr>
				<th>NIK PEMOHON</th>
				<td>' . $user_data['nikPemohon'] . '</td>
			</tr>
			<tr>
				<th>NAMA</th>
				<td>' . $user_data['nama'] . '</td>
			</tr>
			<tr>
				<th>STATUS</th>
				<td>' . $user_data['status'] . '</td>
			</tr>
			<tr>
				<th>JENIS KELAMIN</th>
				<td>' . $user_data['jenisKelamin'] . '</td>
            </tr>
			<tr>
				<th>TTL</th>
				<td>' . $user_data['tempatLahir'] .'</td>
			</tr>
			<tr>
				<th>TTL</th>
				<td>' . $user_data['tanggalLahir'] .'</td>
			</tr>

			<tr>
				<th>AGAMA</th>
				<td>' . $user_data['agama'] . '</td>
            </tr>
			<tr>
				<th>KEWARGANEGARAAN</th>
				<td>' . $user_data['wargaNegara'] . '</td>
			</tr>
			<tr>
				<th>PEKERJAAN</th>
				<td>' . $user_data['pekerjaan'] . '</td>
			</tr>
			<tr>
				<th>ALAMAT</th>
				<td>' . $user_data['tempatTinggal'] . '</td>
			</tr>
			<tr>
				<th>NO S. PENGANTAR</th>
				<td>' . $user_data['noSuratpengantar'] . '</td>
			</tr>
			<tr>
				<th>TGL S. PENGANTAR</th>
				<td>' . $user_data['tglSuratpengantar'] . '</td>
			</tr>
			<tr>
				<th>MAKSUD SURAT</th>
				<td>' . $user_data['maksudSurat'] . '</td>
			</tr>
			<tr>
				<th>YANG MENGETAHUI</th>
				<td>' . $user_data['pejabat'] . '</td>
			</tr>
			<tr>
			
			</tr>

		</table>
		';
    }
}
?>