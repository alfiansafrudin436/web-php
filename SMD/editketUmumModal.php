
<?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, " select * from ketumum where noSurat='$_POST[noSurat]'");
                $user_data = mysqli_fetch_array($view);


?>

            <form role="form" id="form-edit" method="post" action="editketUmumQuery.php">

            <div class="form-group">
            <label class="font-weight-bold">No Surat<sup class="text-danger">*</sup></label>
            <input type="text" name="noSurat" class="form-control" value="<?php echo $user_data['noSurat']; ?>"required readonly/>
            <p style="color:red" id="error_edit_noSurat"></p>
            </div>

            <div class="form-group">
            <label class="font-weight-bold">NIK Pemohon<sup class="text-danger">*</sup></label>
            <input type="text" name="nikPemohon"  class="form-control" value="<?php echo $user_data['nikPemohon']; ?>" required/>
            </div>

            <div class="form-group">
            <label class="font-weight-bold">Nama Pemohon<sup class="text-danger">*</sup></label>
            <input type="text" name="nama" class="form-control" value="<?php echo $user_data['nama']; ?>"required/>
            <p style="color:red" id="error_edit_nama"></p>
            </div>


            <div class="form-group">
                      <label class="font-weight-bold">Status<sup class="text-danger">*</sup></label>
                      <select name="status"  class="form-control" required>
                        <option value="Belum Kawin" <?php if ($user_data['status']=='Belum Kawin') {
    ; ?> selected <?php
}?>>Belum Kawin</option>
                        <option value="Kawin" <?php if ($user_data['status']=='Kawin') {
        ; ?> selected <?php
    }?>>Kawin</option>
                        <option value="Cerai Hidup" <?php if ($user_data['status']=='Cerai Hidup') {
        ; ?> selected <?php
    }?>>Cerai Hidup</option>
                        <option value="Cerai Mati" <?php if ($user_data['status']=='Cerai Mati') {
        ; ?> selected <?php
    }?>>Cerai Mati</option>
                      </select>
                      </div>

            <div class="form-group">
  <label class="font-weight-bold">Jenis Kelamin<sup class="text-danger">*</sup></label>
  <select name="jenisKelamin" class="form-control" required>
                        <option value="Laki-Laki" <?php if ($user_data['jenisKelamin']=='Laki-Laki') {
        ; ?> selected <?php
    }?>>Laki-Laki</option>
                        <option value="Perempuan" <?php if ($user_data['jenisKelamin']=='Perempuan') {
        ; ?> selected <?php
    }?>>Perempuan</option>
                      </select>
</div>
            <div class="form-group">
            <label class="font-weight-bold">Tempat Lahir<sup class="text-danger">*</sup></label>
            <input name="tempatLahir"  class="form-control" value="<?php echo $user_data['tempatLahir']; ?>" required>
            </div>
            <br />
            <div class="form-group">
            <label class="font-weight-bold">Tanggal Lahir<sup class="text-danger">*</sup></label>
            <input  type="date" name="tanggalLahir" class="form-control" value="<?php echo $user_data['tanggalLahir']; ?>"required>
            </div>

            <div class="form-group">
            <label class="font-weight-bold">Kewarganegaraan<sup class="text-danger">*</sup></label>
            <input type="text" name="wargaNegara"  class="form-control" value="<?php echo $user_data['wargaNegara']; ?>"required>
            </div>

            <div class="form-group">
  <label class="font-weight-bold">Agama Ibu</label>
  <select name="agama" class="form-control" required>
                        <option value="Islam" <?php if ($user_data['agama']=='Islam') {
        ; ?> selected <?php
    }?> >Islam</option>
                        <option value="Kristen" <?php if ($user_data['agama']=='Kristen') {
        ; ?> selected <?php
    }?> >Kristen</option>
                        <option value="Katholik" <?php if ($user_data['agama']=='Khatolik') {
        ; ?> selected <?php
    }?> >Katholik</option>
                        <option value="Hindu" <?php if ($user_data['agama']=='Hindu') {
        ; ?> selected <?php
    }?> >Hindu</option>
                        <option value="Budha" <?php if ($user_data['agama']=='Budha') {
        ; ?> selected <?php
    }?> >Budha</option>
                        <option value="Khonghucu" <?php if ($user_data['agama']=='Khonghucu') {
        ; ?> selected <?php
    }?> >Khonghucu</option>
                      </select>
</div>

<div class="form-group">
  <label class="font-weight-bold">Pekerjaan Ibu</label>
  <select name="pekerjaan" class="form-control" required>
  <option value="Belum/Tidak Bekerja" <?php if ($user_data['pekerjaan']=='Belum/Tidak Bekerja') {
        ; ?> selected <?php
    }?>>Belum/Tidak Bekerja</option>
                        <option value="Mengurus Rumah Tangga" <?php if ($user_data['pekerjaan']=='Mengurus Rumah Tangga"') {
        ; ?> selected <?php
    }?>>Mengurus Rumah Tangga</option>
                        <option value="Pelajar/Mahasiswa" <?php if ($user_data['pekerjaan']=='Pelajar/Mahasiswa') {
        ; ?> selected <?php
    }?>>Pelajar/Mahasiswa</option>
                        <option value="Pensiunan" <?php if ($user_data['pekerjaan']=='Pensiunan') {
        ; ?> selected <?php
    }?>>Pensiunan</option>
                        <option value="PNS" <?php if ($user_data['pekerjaan']=='PNS') {
        ; ?> selected <?php
    }?>>PNS</option>
                        <option value="TNI" <?php if ($user_data['pekerjaan']=='TNI') {
        ; ?> selected <?php
    }?>>TNI</option>
                      </select>
                      </div>

            <div class="form-group">
            <label class="font-weight-bold">Alamat<sup class="text-danger">*</sup></label>
            <textarea type="text" name="tempatTinggal" class="form-control"  required><?php echo $user_data['tempatTinggal']; ?></textarea>
            </div>

            <div class="form-group">
            <label class="font-weight-bold">Tanggal Surat Pengantar<sup class="text-danger">*</sup></label>
            <input type="date" name="tglSuratpengantar"  class="form-control" data-toggle="datepicker" value="<?php echo $user_data['tglSuratpengantar']; ?>" required/>
            </div>

            <div class="form-group">
            <label class="font-weight-bold">NO Surat Pengantar<sup class="text-danger">*</sup></label>
            <input type="text" name="noSuratpengantar"  class="form-control" value="<?php echo $user_data['noSuratpengantar']; ?>"required>
            </div>

            <div class="form-group">
            <label class="font-weight-bold">Maksud Surat<sup class="text-danger">*</sup></label>
            <textarea name="maksudSurat" class="form-control" required><?php echo $user_data['maksudSurat']; ?></textarea>
            </div>

            <div class="form-group">
            <label class="font-weight-bold">Pejabat Yang Mengetahui<sup class="text-danger" >*</sup></label>
            <input type="text" name="pejabat" class="form-control" value="<?php echo $user_data['pejabat']; ?>"required>
            </div>

            <br>
            </form>
