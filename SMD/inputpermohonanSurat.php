
<?php
    $nik = htmlspecialchars($_POST['nik']);
    $namaPemohon = htmlspecialchars($_POST['nama']);
    $jenis = htmlspecialchars($_POST['jenisSurat']);
    $keterangan = htmlspecialchars($_POST['keterangan']);
    $status = htmlspecialchars($_POST['status']);
    $id_penduduk = htmlspecialchars($_POST['id_penduduk']);

   


    // include database connection file
    include_once("koneksi.php");
    // if ($ukuran_file <= 20000000) { // Cek apakah ukuran file yang diupload kurang dari sama dengan 1MB
    //     // Jika ukuran file kurang dari sama dengan 1MB, lakukan :
    //     // Proses upload
    //     if (move_uploaded_file($lokasi_file, $path)) {
    //         $result = mysqli_query($mysqli, "INSERT INTO permohonan(nik,namaPemohon,jenis,keterangan,status, id_penduduk, file) VALUES
    //         ('$nik','$namaPemohon','$jenis','$keterangan','$status','$id_penduduk','$nama_file')");
    //         header("location:permohonanSuratPenduduk.php?pesan=input");
    //     } else {
    //         header("location:permohonanSuratPenduduk.php?pesan=gagal");
    //     }
    // } else {
    //     header("location:permohonanSuratPenduduk.php?pesan=gagal");
    // }
    $nama_file = $_FILES['gambar']['name'];
    $ukuran_file = $_FILES['gambar']['size'];
    $tipe_file = $_FILES['gambar']['type'];
    $tmp_file = $_FILES['gambar']['tmp_name'];
    // Set path folder tempat menyimpan gambarnya
    $path = "img/".$nama_file;
    if ($tipe_file == "image/jpeg" || $tipe_file == "image/png") { // Cek apakah tipe file yang diupload adalah JPG / JPEG / PNG
      // Jika tipe file yang diupload JPG / JPEG / PNG, lakukan :
      if ($ukuran_file <= 2000000) { // Cek apakah ukuran file yang diupload kurang dari sama dengan 1MB
        // Jika ukuran file kurang dari sama dengan 1MB, lakukan :
        // Proses upload
        if (move_uploaded_file($tmp_file, $path)) { // Cek apakah gambar berhasil diupload atau tidak
          // Jika gambar berhasil diupload, Lakukan :
          // Proses simpan ke Database
          $query = "INSERT INTO permohonan(nik,namaPemohon,jenis,keterangan,status, id_penduduk, namafile,ukuran,tipe) VALUES       ('$nik','$namaPemohon','$jenis','$keterangan','$status','$id_penduduk','$nama_file','$ukuran_file','$tipe_file')";
            $sql = mysqli_query($mysqli, $query);
            if ($sql) { // Cek jika proses simpan ke database sukses atau tidak
                // Jika Sukses, Lakukan :
                header("location:permohonanSuratPenduduk.php?pesan=input");
            } else {
                // Jika Gagal, Lakukan :
                echo "Maaf, Terjadi kesalahan saat mencoba untuk menyimpan data ke database.";
                header("location:permohonanSuratPenduduk.php?pesan=kesalahan");
            }
        } else {
            // Jika gambar gagal diupload, Lakukan :
            echo "Maaf, Gambar gagal untuk diupload.";
            header("location:permohonanSuratPenduduk.php?pesan=gagal");
        }
      } else {
          // Jika ukuran file lebih dari 1MB, lakukan :
          echo "Maaf, Ukuran gambar yang diupload tidak boleh lebih dari 1MB";
          header("location:permohonanSuratPenduduk.php?pesan=ukuran");
      }
    } else {
        // Jika tipe file yang diupload bukan JPG / JPEG / PNG, lakukan :
        echo "Maaf, Tipe gambar yang diupload harus JPG / JPEG / PNG.";
        header("location:permohonanSuratPenduduk.php?pesan=tipe");
    }
    ?>

