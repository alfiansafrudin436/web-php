
<?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, " select *
                                                  from
                                                  penduduk
                                                  where id= $_POST[id]");
                 $user_data = mysqli_fetch_array($view);
?>

            <form role="form" id="form-edit" method="post" action="editPendudukQuery.php">
                      <div class="form-group">
                      <input type="hidden" name="id" value="<?php echo $user_data['id'] ; ?>">
                      </div>


                      <div class="form-group">
                      <label class="font-weight-bold">Status</label>
                      <div>
                      <input type="radio" name="status" value="Hidup"  <?php if ($user_data['status']=='Hidup') {
    ; ?> checked <?php
} ?> >Hidup
                      <input type="radio" name="status" value="Meninggal" <?php if ($user_data['status']=='Meninggal') {
        ; ?> checked <?php
    } ?> >Meninggal
                      </div>
                      </div>


                      <div class="form-group">
                      <label class="font-weight-bold">NIK</label>
                      <input type="text" name="nik" class="form-control" value="<?php echo $user_data['nik']; ?>" readonly/>
                      <p style="color:red" id="error_edit_nik"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Nama<sup class="text-danger">*</sup></label>
                      <input type="text" name="nama" class="form-control" value="<?php echo $user_data['nama']; ?>" required/>
                      <p style="color:red" id="error_edit_nama"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Nomor KK<sup class="text-danger">*</sup></label>
                      <input type="text" name="noKK" class="form-control" value="<?php echo $user_data['noKK']; ?>" required/>
                      <p style="color:red" id="error_edit_noKK"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Alamat<sup class="text-danger">*</sup></label>
                      <textarea name="alamat" class="form-control" required><?php echo $user_data['alamat']; ?></textarea>
                      <p style="color:red" id="error_edit_alamat"></p>
                      </div>

                      <div class="form-group">
                        <label class="font-weight-bold">Dusun<sup class="text-danger">*</sup></label>
                        <select name="dusun" id="dusun" class="form-control" required>
                            <option value="">--Pilih--</option>
                            <option value="Soropati" <?php if ($user_data['dusun']=='Soropati') {
        ; ?> selected <?php
    }?>>Soropati</option>
                            <option value="Sekendal" <?php if ($user_data['dusun']=='Sekendal') {
        ; ?> selected <?php
    }?>>Sekendal</option>
                            <option value="Segajih" <?php if ($user_data['dusun']=='Segajih') {
        ; ?> selected <?php
    }?>>Segajih</option>
                            <option value="Keji" <?php if ($user_data['dusun']=='Keji') {
        ; ?> selected <?php
    }?>>Keji</option>
                            <option value="Teganing I" <?php if ($user_data['dusun']=='Teganing I') {
        ; ?> selected <?php
    }?>>Teganing I</option>
                            <option value="Teganing II" <?php if ($user_data['dusun']=='Teganing II') {
        ; ?> selected <?php
    }?>>Teganing II</option>
                            <option value="Teganing III" <?php if ($user_data['dusun']=='Teganing III') {
        ; ?> selected <?php
    }?>>Teganing III</option>
                            <option value="Tirto" <?php if ($user_data['dusun']=='Tirto') {
        ; ?> selected <?php
    }?>>Tirto</option>
                            <option value="Crangah" <?php if ($user_data['dusun']=='Crangah') {
        ; ?> selected <?php
    }?>>Crangah</option>
                            <option value="Sungapan I" <?php if ($user_data['dusun']=='Sungapan I') {
        ; ?> selected <?php
    }?>>Sungapan I</option>
                            <option value="Sungapan II" <?php if ($user_data['dusun']=='Sungapan II') {
        ; ?> selected <?php
    }?>>Sungapan II</option>
                            <option value="Menguri" <?php if ($user_data['dusun']=='Menguri') {
        ; ?> selected <?php
    }?>>Menguri</option>
                            <option value="Sebatang" <?php if ($user_data['dusun']=='Sebatang') {
        ; ?> selected <?php
    }?>>Sebatang</option>
                            <option value="Nganti" <?php if ($user_data['dusun']=='Nganti') {
        ; ?> selected <?php
    }?>>Nganti</option>
                        </select>
                    </div>

                      <div class="form-group">
                      <label class="font-weight-bold">RT<sup class="text-danger">*</sup></label>
                      <input type="text" name="rt" class="form-control" value="<?php echo $user_data['rt']; ?>" required/>
                      <p style="color:red" id="error_edit_rt"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">RW<sup class="text-danger">*</sup></label>
                      <input type="text" name="rw" class="form-control" value="<?php echo $user_data['rw']; ?>" required/>
                      <p style="color:red" id="error_edit_rw"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Jenis Kelamin<sup class="text-danger">*</sup></label>
                      <select name="jenisKelamin" class="form-control" required>
                        <option value="Laki-Laki" <?php if ($user_data['jenisKelamin']=='Laki-Laki') {
        ; ?> selected <?php
    }?>>Laki-Laki</option>
                        <option value="Perempuan" <?php if ($user_data['jenisKelamin']=='Perempuan') {
        ; ?> selected <?php
    }?>>Perempuan</option>
                      </select>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Tempat Lahir<sup class="text-danger">*</sup></label>
                      <input type="text" name="tempatLahir" class="form-control" value="<?php echo $user_data['tempatLahir']; ?>" required/>
                      <p style="color:red" id="error_edit_tempatLahir"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Tanggal Lahir<sup class="text-danger">*</sup></label>
                      <input type="date" name="tanggalLahir"class="form-control" value="<?php echo $user_data['tanggalLahir']; ?>" required/>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Agama<sup class="text-danger">*</sup></label>
                      <select name="agama" class="form-control" required>
                        <option value="Islam" <?php if ($user_data['agama']=='Islam') {
        ; ?> selected <?php
    }?> >Islam</option>
                        <option value="Kristen" <?php if ($user_data['agama']=='Kristen') {
        ; ?> selected <?php
    }?> >Kristen</option>
                        <option value="Katholik" <?php if ($user_data['agama']=='Khatolik') {
        ; ?> selected <?php
    }?> >Katholik</option>
                        <option value="Hindu" <?php if ($user_data['agama']=='Hindu') {
        ; ?> selected <?php
    }?> >Hindu</option>
                        <option value="Budha" <?php if ($user_data['agama']=='Budha') {
        ; ?> selected <?php
    }?> >Budha</option>
                        <option value="Khonghucu" <?php if ($user_data['agama']=='Khonghucu') {
        ; ?> selected <?php
    }?> >Khonghucu</option>
                      </select>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Kewarganegaraan<sup class="text-danger">*</sup></label>
                      <input type="text" name="kewarganegaraan"class="form-control" value="<?php echo $user_data['kewarganegaraan']; ?>" required/>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Pendidikan<sup class="text-danger">*</sup></label>
                      <select name="pendidikan" class="form-control" required>
                        <option value="Tidak/Belum Sekolah"<?php if ($user_data['pendidikan']=='Tidak/Belum Sekolah') {
        ; ?> selected <?php
    }?>>Tidak/Belum Sekolah</option>
                        <option value="Belum Tamat SD/Sederajat"<?php if ($user_data['pendidikan']=='Belum Tamat SD/Sederajat') {
        ; ?> selected <?php
    }?>>Belum Tamat SD/Sederajat</option>
                        <option value="SD/Sederajat" <?php if ($user_data['pendidikan']=='SD/Sederajat') {
        ; ?> selected <?php
    }?>>SD/Sederajat</option>
                        <option value="SLTP/Sederajat" <?php if ($user_data['pendidikan']=='SLTP/Sederajat') {
        ; ?> selected <?php
    }?>>SLTP/Sederajat</option>
                        <option value="SLTA/Sederajat" <?php if ($user_data['pendidikan']=='SLTA/Sederajat') {
        ; ?> selected <?php
    }?>>SLTA/Sederajat</option>
                        <option value="D3" <?php if ($user_data['pendidikan']=='D3') {
        ; ?> selected <?php
    }?>>D3</option>
                        <option value="S1" <?php if ($user_data['pendidikan']=='S1') {
        ; ?> selected <?php
    }?>>S1</option>
                        <option value="S2" <?php if ($user_data['pendidikan']=='S2') {
        ; ?> selected <?php
    }?>>S2</option>
                        <option value="S3" <?php if ($user_data['pendidikan']=='S3') {
        ; ?> selected <?php
    }?>>S3</option>
                      </select>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Pekerjaan<sup class="text-danger">*</sup></label>
                      <select name="pekerjaan" class="form-control" required>
                        <option value="Belum/Tidak Bekerja" <?php if ($user_data['pekerjaan']=='Belum/Tidak Bekerja') {
        ; ?> selected <?php
    }?>>Belum/Tidak Bekerja</option>
                        <option value="Mengurus Rumah Tangga" <?php if ($user_data['pekerjaan']=='Mengurus Rumah Tangga"') {
        ; ?> selected <?php
    }?>>Mengurus Rumah Tangga</option>
                        <option value="Pelajar/Mahasiswa" <?php if ($user_data['pekerjaan']=='Pelajar/Mahasiswa') {
        ; ?> selected <?php
    }?>>Pelajar/Mahasiswa</option>
                        <option value="Pensiunan" <?php if ($user_data['pekerjaan']=='Pensiunan') {
        ; ?> selected <?php
    }?>>Pensiunan</option>
                        <option value="PNS" <?php if ($user_data['pekerjaan']=='PNS') {
        ; ?> selected <?php
    }?>>PNS</option>
                        <option value="TNI" <?php if ($user_data['pekerjaan']=='TNI') {
        ; ?> selected <?php
    }?>>TNI</option>
                        <option value="Petani"<?php if ($user_data['pekerjaan']=='Petani') {
        ; ?> selected <?php
    }?>>Petani</option>
                        <option value="Buruh"<?php if ($user_data['pekerjaan']=='Buruh') {
        ; ?> selected <?php
    }?>>Buruh</option>
                        <option value="Wiraswasta"<?php if ($user_data['pekerjaan']=='Wiraswasta') {
        ; ?> selected <?php
    }?>>Wiraswasta</option>
                      </select>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Pengahasilan</label>
                      <input type="text" name="penghasilan" class="form-control" value="<?php echo $user_data['penghasilan']; ?>"/>
                      <p style="color:red" id="error_edit_penghasilan"></p>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Status dalam Keluarga<sup class="text-danger">*</sup></label>
                    <select name="statusdalamKeluarga" class="form-control" required>
                        <option value="Kepala Keluarga" <?php if ($user_data['statusdalamKeluarga']=='Kepala Keluarga') {
        ; ?> selected <?php
    }?>>Kepala Keluarga</option>
                        <option value="Suami" <?php if ($user_data['statusdalamKeluarga']=='Suami') {
        ; ?> selected <?php
    }?>>Suami</option>
                        <option value="Istri" <?php if ($user_data['statusdalamKeluarga']=='Istri') {
        ; ?> selected <?php
    }?>>Istri</option>
                        <option value="Anak" <?php if ($user_data['statusdalamKeluarga']=='Anak') {
        ; ?> selected <?php
    }?>>Anak</option>
                        <option value="Menantu" <?php if ($user_data['statusdalamKeluarga']=='Menantu') {
        ; ?> selected <?php
    }?>>Menantu</option>
                        <option value="Cucu" <?php if ($user_data['statusdalamKeluarga']=='Cucu') {
        ; ?> selected <?php
    }?>>Cucu</option>
                    </select>
                    </div>

                    <div class="form-group">
                      <label class="font-weight-bold">Status Pernikahan<sup class="text-danger">*</sup></label>
                      <select name="statusPernikahan"  class="form-control" required>
                        <option value="Belum Kawin" <?php if ($user_data['statusPernikahan']=='Belum Kawin') {
        ; ?> selected <?php
    }?>>Belum Kawin</option>
                        <option value="Kawin" <?php if ($user_data['statusPernikahan']=='Kawin') {
        ; ?> selected <?php
    }?>>Kawin</option>
                        <option value="Cerai Hidup" <?php if ($user_data['statusPernikahan']=='Cerai Hidup') {
        ; ?> selected <?php
    }?>>Cerai Hidup</option>
                        <option value="Cerai Mati" <?php if ($user_data['statusPernikahan']=='Cerai Mati') {
        ; ?> selected <?php
    }?>>Cerai Mati</option>
                      </select>
                      </div>

                      <div class="form-group">
                      <label class="font-weight-bold">Golongan Darah</label>
                      <select name="golDarah"  class="form-control" >
                      <option value="-" <?php if ($user_data['golDarah']=='-') {
        ; ?> selected <?php
    }?>>-</option>
                        <option value="A" <?php if ($user_data['golDarah']=='A') {
        ; ?> selected <?php
    }?>>A</option>
                        <option value="AB" <?php if ($user_data['golDarah']=='AB') {
        ; ?> selected <?php
    }?>>AB</option>
                        <option value="B" <?php if ($user_data['golDarah']=='B') {
        ; ?> selected <?php
    }?>>B</option>
                        <option value="O" <?php if ($user_data['golDarah']=='O') {
        ; ?> selected <?php
    }?>>O</option>
                      </select>
                      </div>
                      <br>



            </form>

