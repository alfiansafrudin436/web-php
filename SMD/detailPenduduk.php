<?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, " select * from penduduk where id= $_POST[id]");

    if ($view->num_rows) {
        //fetch data ke dalam veriabel $user_data
        $user_data = $view->fetch_assoc();
        //menampilkan data dengan table
        echo '
		<p>Berikut ini adalah detail dari data penduduk dengan nik <b>' . $user_data['nik'] . '</b></p>
		<table class="table table-bordered">
			<tr>
				<th>NIK</th>
				<td>' . $user_data['nik'] . '</td>
			</tr>
			<tr>
				<th>NAMA</th>
				<td>' . $user_data['nama'] . '</td>
			</tr>
			<tr>
				<th>NO KK</th>
				<td>' . $user_data['noKK'] . '</td>
            </tr>
			<tr>
				<th>ALAMAT</th>
				<td>' . $user_data['alamat'] . '</td>
			</tr>
			<tr>
				<th>DUSUN</th>
				<td>' . $user_data['dusun'] . '</td>
            </tr>
			<tr>
				<th>RT</th>
				<td>' . $user_data['rt'] . '</td>
            </tr>
			<tr>
				<th>RW</th>
				<td>' . $user_data['rw'] . '</td>
			</tr>
			<tr>
				<th>JENIS KELAMIN</th>
				<td>' . $user_data['jenisKelamin'] . '</td>
			</tr>
			<tr>
				<th>TEMPAT LAHIR</th>
				<td>' . $user_data['tempatLahir'] . '</td>
            </tr>
			<tr>
				<th>TANGGAL LAHIR</th>
				<td>' . $user_data['tanggalLahir'] . '</td>
			</tr>            
			<tr>
				<th>AGAMA</th>
				<td>' . $user_data['agama'] . '</td>
			</tr>

			<tr>
				<th>KEWARGANEGARAAN</th>
				<td>' . $user_data['kewarganegaraan'] . '</td>
			</tr>

			<tr>
				<th>PENDIDIKAN</th>
				<td>' . $user_data['pendidikan'] . '</td>
			</tr>
			<tr>
				<th>PEKERJAAN</th>
				<td>' . $user_data['pekerjaan'] . '</td>
			</tr>
			<tr>
				<th>PENGHASILAN</th>
				<td>' . $user_data['penghasilan'] . '</td>
			</tr>
			<tr>
				<th>STATUS DALAM KELUARGA</th>
				<td>' . $user_data['statusdalamKeluarga'] . '</td>
			</tr>
			<tr>
				<th>STATUS PERNIKAHAN</th>
				<td>' . $user_data['statusPernikahan'] . '</td>
			</tr>
			<tr>
				<th>GOL DARAH</th>
				<td>' . $user_data['golDarah'] . '</td>
			</tr>

		</table>
		';
    }
