<br>
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>Alamat</th>
                      <th>JK</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>Alamat</th>
                      <th>JK</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
            <?php
                include"koneksi.php";
                $no = 1;
                $view = mysqli_query($mysqli, " select
                                                        id,
                                                        nama,
                                                        nik,
                                                        alamat,
                                                        jenisKelamin,
                                                        status
                                                  from
                                                  penduduk
                                                  where status Like'%Meninggal%'");
                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
        <tr>
            <td>
                <?php echo $no++; ?>
            </td>
            <td>
                <?php echo $user_data['nama']; ?>
            </td>
            <td>
                <?php echo $user_data['nik']; ?>
            </td>
            <td>
                <?php echo $user_data['alamat']; ?>
            </td>
            <td>
                <?php echo $user_data['jenisKelamin']; ?>
            </td>
            <td>
                <?php echo $user_data['status']; ?>
            </td>
            <td>
            <center>
                <button title="Detail" class="btn btn-primary btn-xs" id="btndetail" data-id=<?php echo $user_data['id']?> ><i class="fas fa-info-circle"></i></button>
                <button title="Ubah" class="btn btn-success btn-xs" id="btneditPenduduk" data-id=<?php echo $user_data['id']?> ><i class="far fa-edit"></i></button>
            <center>
            </td>
        </tr>
        <?php
                }
        ?>
        </tbody>
    </table>

    <script src="js/demo/datatables-demo.js"></script>
