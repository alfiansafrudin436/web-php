<br>
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>Jenis Surat</th>
                      <th>Keterangan</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>Jenis Surat</th>
                      <th>Keterangan</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody> 
            <?php
                include"koneksi.php";
                $no = 1;
                $view = mysqli_query($mysqli, " select * from permohonan order by nik");
                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
        <tr>
            <td>
                <?php echo $no++; ?>
            </td>
            <td>
                <?php echo $user_data['nik']; ?>
            </td>
            <td>
                <?php echo $user_data['namaPemohon']; ?>
            </td>
            <td>
                <?php echo $user_data['jenis']; ?>
            </td>
            <td>
                <?php echo $user_data['keterangan']; ?>
            </td>
            <td>
                <?php echo $user_data['status']; ?>
            </td>
            <td>
            <center>
            <?php
                    echo'
                            <button title="Detail" type="button" class="btn btn-primary btn-xs" id="btndetail" data-id="'.$user_data['id'].'"><i class="fas fa-info-circle"></i></button>
                            <button title="Ubah Status" type="button" class="btn btn-success btn-xs" id="btnedit" data-id="'.$user_data['id'].'" ><i class="far fa-edit"></i></button>
                            <button title="Hapus" type="button" class="btn btn-danger btn-xs" id="btnhapus" data-id="'.$user_data['id'].'" ><i class="fas fa-trash"></i></button>
                            

                    '; ?>
            <center>
            </td>
        </tr>
        <?php
                }
        ?>
        </tbody>
    </table>

    <script src="js/demo/datatables-demo.js"></script>
