<?php
session_start();
include'koneksi.php';
require('pdf/fpdf.php');

$noSurat=$_GET['id'];
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}



//$noSurat = $_GET["noSurat"];

$result = mysqli_query($mysqli, "SELECT * FROM ketumum WHERE noSurat ='$noSurat'");
if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();
}
$query = mysqli_fetch_assoc($result);
$tanggalLahir = $query['tanggalLahir'];
$tanggalLahir = tgl_indo(date('Y-m-d', strtotime($tanggalLahir)));
$tanggalSuratpengantar = $query['tglSuratpengantar'];
$tanggalSuratpengantar = tgl_indo(date('Y-m-d', strtotime($tanggalSuratpengantar)));
$hariIni=tgl_indo(date('Y-m-d'));


$pdf = new FPDF("P", "cm", "A4");
$pdf->SetMargins(2, 1, 1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Image('img/logo.png', 1, 1, 3, 3);
$pdf->SetFont('Times', 'B', 16);
$pdf->SetX(2);
$pdf->MultiCell(18, 0.7, 'PEMERINTAH KABUPATEN KULON PROGO', 0, 'C');
$pdf->MultiCell(18, 0.7, 'PANEWON KOKAP', 0, 'C');
$pdf->SetFont('Times', 'B', 20);
$pdf->SetX(2);
$pdf->MultiCell(18, 0.7, 'KALURAHAN HARGOTIRTO', 0, 'C');
$pdf->SetX(2);
$pdf->SetFont('Arial', 'B', 10);
$pdf->SetX(2);
$pdf->MultiCell(18, 0.5, 'Sekendal, Hargotirto, Kec. Kokap, Kabupaten Kulonprogo, DIY - Kodepos 55653', 0, 'C');
$pdf->SetX(2);
$pdf->MultiCell(18, 0.5, 'Telp : 08112635610   Email : desahargotirto@yahoo.co.id', 0, 'C');
$pdf->Line(1, 4.2, 20, 4.2);
$pdf->SetLineWidth(0.1);
$pdf->Line(1, 4.3, 20, 4.3);
$pdf->SetLineWidth(0);
$pdf->SetFont('Arial', 'B', 13);
$pdf->Cell(18, 2, "SURAT KETERANGAN", 0, 10, 'C');
$pdf->Line(8, 5.5, 14, 5.5);
$pdf->SetLineWidth(0.1);
$pdf->SetX(8.5);
$pdf->Cell(0, 0, 'NOMOR :', 0, 0, 'L');
$pdf->SetX(11);
$pdf->Cell(0, 0, $query['noSurat'], 0, 0, 'L');
$pdf->SetX(5);
$pdf->SetFont('Arial', '', 12);
$pdf->ln(2);
$pdf->SetX(3);
$pdf->Cell(0, 1, "Yang bertanda tangan dibawah ini Kepala Desa Hargotirto Kec. Kokap Kabupaten ", 0, 3, 'L');
$pdf->SetX(2);
$pdf->Cell(0, 0, "Kulonprogo. Menerangkan dengan sebenarnya bahwa : ", 0, 3, 'L');
$pdf->ln(1);
$pdf->SetX(4);
$pdf->Cell(0, 0, 'Nama', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 0, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 0, $query['nama'], 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 1, 'Jenis Kelamin', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 1, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 1, $query['jenisKelamin'], 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 2, 'Tanggal Lahir', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 2, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 2, $tanggalLahir, 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 3, 'Tempat Lahir', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 3, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 3, $query['tempatLahir'], 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 4, 'Agama', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 4, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 4, $query['agama'], 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 5, 'Pekerjaan', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 5, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 5, $query['pekerjaan'], 0, 0, 'L');
$pdf->SetX(4);
$pdf->Cell(0, 6, 'Alamat', 0, 0, 'L');
$pdf->SetX(6.8);
$pdf->Cell(0, 6, ':', 0, 0, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 6, $query['tempatTinggal'], 0, 0, 'L');
$pdf->ln(3.5);
$pdf->SetX(3);
$pdf->Cell(0, 1, "Surat Keterangan ini diberikan berdasarkan dan pertimbangkan Surat ", 0, 3, 'L');
$pdf->SetX(2);
$pdf->Cell(0, 0, "Pengantar RT/RW Nomor: ", 0, 3, 'L');
$pdf->SetX(7);
$pdf->Cell(0, 0, $query['noSuratpengantar'], 0, 3, 'L');
$pdf->SetX(8.5);
$pdf->Cell(0, 0, "tertanggal", 0, 3, 'L');
$pdf->SetX(10.5);
$pdf->Cell(0, 0, $tanggalSuratpengantar, 0, 3, 'L');
$pdf->SetX(2);
$pdf->Cell(0, 2, $query['maksudSurat'], 0, 3, 'L');
$pdf->ln(2);
$pdf->SetX(13);
$pejabat=$query['pejabat'];
$q=mysqli_query($mysqli, "Select * from pegawai where nama ='$pejabat'");
$data=mysqli_fetch_assoc($q);
if ($data['jabatan']=='Lurah') {
    $pdf->Cell(0, 0, $hariIni, 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $data['jabatan'], 0, 0, 'C');
    $pdf->ln(3);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $pejabat, 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $data['nip'], 0, 0, 'C');
} else {
    $pdf->Cell(0, 0, $hariIni, 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, 'An. Lurah', 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $data['jabatan'], 0, 0, 'C');
    $pdf->ln(3);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $pejabat, 0, 0, 'C');
    $pdf->ln(0.5);
    $pdf->SetX(13);
    $pdf->Cell(0, 0, $data['nip'], 0, 0, 'C');
}
$pdf->Output("ketUmum.pdf", "I");
