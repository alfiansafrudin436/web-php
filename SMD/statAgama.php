<?php


    session_start();

    if (!isset($_SESSION["username"])) {
        echo "<script>alert('Anda harus login dulu');window.location='index.php'</script>";
        exit;
    }

    $username=$_SESSION["username"];
    ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Statistik</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="Dashboard.php">
      <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SMD<sup>Hargotirto</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="Dashboard.php">
        <i class="fas fa-home"></i>
          <span>Halaman Utama</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Menu
      </div>

      <!-- Nav Item - Data Penunjang -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePenunjang" aria-expanded="true" aria-controls="collapsePenunjang">
        <i class="fas fa-server"></i>
          <span>Data Penunjang</span>
        </a>
        <div id="collapsePenunjang" class="collapse" aria-labelledby="headingPenunjang" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data Penunjang</h6>
            <a class="collapse-item" href="profilDesa.php">Profil Desa</a>
            <a class="collapse-item" href="visiMisi.php">Visi Misi</a>
            <a class="collapse-item" href="Pegawai.php">Pegawai</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Kependudukan -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseKependudukan" aria-expanded="true" aria-controls="collapseKependudukan">
        <i class="fas fa-database"></i>
          <span>Kependudukan</span>
        </a>
        <div id="collapseKependudukan" class="collapse" aria-labelledby="headingKependudukan" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Kependudukan</h6>
            <a class="collapse-item" href="semuaPenduduk.php">Semua Penduduk</a>
            <a class="collapse-item" href="pendudukKK.php">Berdasarkan KK</a>
            <a class="collapse-item" href="pendudukMeninggal.php">Penduduk Meninggal</a>
          </div>
        </div>
      </li>

      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseStatistik" aria-expanded="true" aria-controls="collapseStatistik">
        <i class="fas fa-database"></i>
          <span>Statistik</span>
        </a>
        <div id="collapseStatistik" class="collapse show" aria-labelledby="headingStatistik" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Statistik</h6>
            <a class="collapse-item" href="statPendidikan.php">Pendidikan</a>
            <a class="collapse-item" href="statPekerjaan.php">Pekerjaan</a>
            <a class="collapse-item active" href="statAgama.php">Agama</a>
            <a class="collapse-item" href="statJK.php">Jenis Kelamin</a>
          </div>
        </div>
      </li>


       <!-- Nav Item - Permintaan Surat -->
       <li class="nav-item">
        <a class="nav-link" href="dataPermohonan.php">
        <i class="fas fa-envelope"></i>
          <span>Permintaan Surat</span></a>
      </li>

      <!-- Nav Item - Layanan Surat -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSurat" aria-expanded="true" aria-controls="collapseSurat">
        <i class="fas fa-envelope-open-text"></i>
          <span>Layanan Surat</span>
        </a>
        <div id="collapseSurat" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Layanan Surat</h6>
            <a class="collapse-item" href="ketMeninggal.php">Ket Meninggal</a>
            <a class="collapse-item" href="ketUmum.php">Ket Umum</a>
            <a class="collapse-item" href="ketKelahiran.php">Ket Kelahiran</a>
          </div>
        </div>
      </li>



      <!-- Nav Item - Mutasi -->
      <li class="nav-item">
        <a class="nav-link" href="dataMutasi.php">
        <i class="fas fa-fw fa-plane"></i>
          <span>Mutasi</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="rounded-circle border-0">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $username?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Keluar
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- End of Topbar -->



    <div class="container-fluid">
      <div class="row">
        <div class="row"></div>
            <!-- Begin Page Content -->
          <div class="container-fluid">
            <h1 class="h3 mb-2 text-gray-800">Statistik Penduduk berdasarkan Pendidikan</h1>
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Tabel Statistik Penduduk berdasarkan Pendidikan</h6>
                </div>
                <div class="card-body">

                  <div class="table-responsive">
                  <div id="dataPenduduk"></div>
                    <!--TEMPAT TABEL DIPANGGIL-->
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
 <!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin untuk keluar?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih "Keluar" jika anda ingin mengakhiri sesi anda.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" href="logout.php">Keluar</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

    <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <script src="script/scriptstatAgama.js"></script> 

</body>

</html>
