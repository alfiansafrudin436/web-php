<?php


    session_start();

    if (!isset($_SESSION["username"])) {
        echo "<script>alert('Anda harus login dulu');window.location='index.php'</script>";
        exit;
    }

    $username=$_SESSION["username"];

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Mutasi</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/datepicker.css" />

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="Dashboard.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SMD<sup>Hargotirto</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item ">
        <a class="nav-link" href="Dashboard.php">
          <i class="fas fa-home"></i>
          <span>Halaman Utama</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Menu
      </div>

      <!-- Nav Item - Data Penunjang -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePenunjang" aria-expanded="true" aria-controls="collapsePenunjang">
        <i class="fas fa-server"></i>
          <span>Data Penunjang</span>
        </a>
        <div id="collapsePenunjang" class="collapse" aria-labelledby="headingPenunjang" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data Penunjang</h6>
            <a class="collapse-item" href="profilDesa.php">Profil Desa</a>
            <a class="collapse-item" href="visiMisi.php">Visi Misi</a>
            <a class="collapse-item" href="Pegawai.php">Pegawai</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Kependudukan -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseKependudukan" aria-expanded="true" aria-controls="collapseKependudukan">
          <i class="fas fa-database"></i>
          <span>Kependudukan</span>
        </a>
        <div id="collapseKependudukan" class="collapse" aria-labelledby="headingKependudukan" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Kependudukan</h6>
            <a class="collapse-item" href="semuaPenduduk.php">Semua Penduduk</a>
            <a class="collapse-item" href="pendudukKK.php">Berdasarkan KK</a>
            <a class="collapse-item" href="pendudukMeninggal.php">Penduduk Meninggal</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseStatistik" aria-expanded="true" aria-controls="collapseStatistik">
        <i class="fas fa-database"></i>
          <span>Statistik</span>
        </a>
        <div id="collapseStatistik" class="collapse" aria-labelledby="headingStatistik" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Statistik</h6>
            <a class="collapse-item" href="statPendidikan.php">Pendidikan</a>
            <a class="collapse-item" href="statPekerjaan.php">Pekerjaan</a>
            <a class="collapse-item" href="statAgama.php">Agama</a>
            <a class="collapse-item" href="statJK.php">Jenis Kelamin</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="dataPermohonan.php">
          <i class="fas fa-envelope"></i>
          <span>Permintaan Surat</span></a>
      </li>

      <!-- Nav Item - Layanan Surat -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSurat" aria-expanded="true" aria-controls="collapseSurat">
          <i class="fas fa-envelope-open-text"></i>
          <span>Layanan Surat</span>
        </a>
        <div id="collapseSurat" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Layanan Surat</h6>
            <a class="collapse-item" href="ketMeninggal.php">Ket Meninggal</a>
            <a class="collapse-item" href="ketUmum.php">Ket Umum</a>
            <a class="collapse-item" href="ketKelahiran.php">Ket Kelahiran</a>
          </div>
        </div>
      </li>


      <!-- Nav Item - Mutasi -->
      <li class="nav-item active">
        <a class="nav-link" href="dataMutasi.php">
          <i class="fas fa-fw fa-plane"></i>
          <span>Mutasi</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

    </ul>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="rounded-circle border-0">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $username?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Keluar
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Mutasi</h1>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Mutasi</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <div align="left">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_data_Modal">Buat Mutasi</button>
                </div>
                <div id="dataMutasi"></div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin untuk keluar?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih "Keluar" jika anda ingin mengakhiri sesi anda.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
          <a class="btn btn-primary" href="logout.php">Keluar</a>
        </div>
      </div>
    </div>
  </div>

  <!--EDIT MODAL-->
  <div id="modal-edit" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content modal-lg">
        <form role="form" id="form-edit" method="post" action="inputMutasi.php">
          <div class="modal-header">
            <h4 class="modal-title">Ubah Data Mutasi</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body modal-lg">
            <div id="data-edit"></div>
          </div>
          <div class="modal-footer modal-lg">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!--INSERT MODAL-->
  <div id="add_data_Modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header ">
          <h4 class="modal-title-bold">Buat Mutasi</h4>
          <div align="right">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        </div>
        <div class="modal-body">
          <form role="form" id="form-tambah" method="post" action="inputMutasi.php">

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">No Surat <sup class="text-danger">*</sup></label>
              <input type="text" id="noMutasi" name="noMutasi" class="form-control" required>
              <p style="color:red" id="error_noMutasi"></p>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Jenis Mutasi<sup class="text-danger">*</sup></label>
              <select name="jenisMutasi" id="jenisMutasi" class="form-control" required>
              <option value="">--Pilih--</option> 
                <option value="Pindah Datang">Pindah Datang</option>
                <option value="Pendidikan">Pindah Keluar</option>
              </select>
            </div>
            </div>
          </div>

            <input type="hidden" name="username" id="username" value="<?php echo $username;?>" class="form-control" required/>

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Kategori Kepindahan<sup class="text-danger">*</sup></label>
              <select name="kategoriPindah" id="kategoriPindah" class="form-control" required>
              <option value="">--Pilih--</option>
                <option value="Antar Desa/ Kelurahan dalam Satu Kota">Antar Desa/ Kelurahan dalam Satu Kota</option>
                <option value="Antar Desa/ Kelurahan dalam Satu Provinsi">Antar Desa/ Kelurahan dalam Satu Provinsi</option>
                <option value="Antar Kota/ Kabupaten/ Provinsi">Antar Kota/ Kabupaten/ Provinsi</option>
              </select>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">NIK Pemohon<sup class="text-danger">*</sup></label>
              <input type="text" id="nik" name="nik" class="form-control" required/>
            </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Nomor KK<sup class="text-danger">*</sup></label>
              <input type="text" id="nomorKK" name="nomorKK" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Nama Kepala Keluarga<sup class="text-danger">*</sup></label>
              <input type="text" id="kepalaKeluarga" name="kepalaKeluarga" class="form-control" required/>
            </div>
            </div>
          </div>


            <div class="form-group">
              <label class="font-weight-bold">Alamat Asal<sup class="text-danger">*</sup></label>
              <textarea id="alamatAsal" name="alamatAsal" class="form-control" required></textarea>
            </div>


          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">RT Asal</label>
              <input type="text" id="rtAsal" name="rtAsal" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">RW Asal</sup></label>
              <input type="text" id="rwAsal" name="rwAsal" class="form-control" required/>
            </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Provinsi<sup class="text-danger">*</sup></label>
              <input type="text" id="provinsi" name="provinsi" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Kabupaten/Kota<sup class="text-danger">*</sup></label>
              <input type="text" id="kabupaten" name="kabupaten" class="form-control" />
            </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Kecamatan<sup class="text-danger">*</sup></label>
              <input type="text" id="kecamatan" name="kecamatan" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Desa Asal<sup class="text-danger">*</sup></label>
              <input type="text" id="desaAsal" name="desaAsal" class="form-control" required/>
            </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Dusun Asal<sup class="text-danger">*</sup></label>
              <input type="text" id="dusunAsal" name="dusunAsal" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Kodepos Asal</label>
              <input type="text" id="kodeposAsal" name="kodeposAsal" class="form-control" required/>
            </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Telp Asal</label>
              <input type="text" id="telpAsal" name="telpAsal" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Nama Pemohon<sup class="text-danger">*</sup></label>
              <input type="text" id="namaPemohon" name="namaPemohon" class="form-control" required/>
            </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Alasan Pindah<sup class="text-danger">*</sup></label>
              <select name="alasanPindah" id="alasanPindah" class="form-control" required>
              <option value="">--Pilih--</option>
                <option value="Pekerjaan">Pekerjaan</option>
                <option value="Pendidikan">Pendidikan</option>
                <option value="Keluarga">Keluarga</option>
                <option value="Lainnya">Lainnya</option>
              </select>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Alasan Permohonan<sup class="text-danger">*</sup></label>
              <select name="alasanMohon" id="alasanMohon" class="form-control" required>
              <option value="">--Pilih--</option>
                <option value="Membentuk Rumah Tangga Baru">Membentuk Rumah Tangga Baru</option>
                <option value="KK Hilang/Rusak">KK Hilang/Rusak</option>
                <option value="Lainnya">Lainnya</option>
              </select>
            </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Nomor KK Tujuan<sup class="text-danger">*</sup></label>
              <input type="text" id="noKKTujuan" name="noKKTujuan" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">NIK Kepala Keluarga Tujuan<sup class="text-danger">*</sup></label>
              <input type="text" id="nikKKTujuan" name="nikKKTujuan" class="form-control" required/>
            </div>
            </div>
          </div>


            <div class="form-group">
              <label class="font-weight-bold">Alamat Tujuan<sup class="text-danger">*</sup></label>
              <textarea id="alamatPindah" name="alamatPindah" class="form-control"></textarea>
            </div>

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Tanggal<sup class="text-danger">*</sup></label>
              <input type="text" name="tanggalKedatangan" id="tanggalKedatangan" class="form-control" data-toggle="datepicker"  required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">RT Tujuan</label>
              <input type="text" id="rtPindah" name="rtPindah" class="form-control" required/>
            </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">RW Tujuan</label>
              <input type="text" id="rwPindah" name="rwPindah" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Provinsi Tujuan<sup class="text-danger">*</sup></label>
              <input type="text" id="provinsiPindah" name="provinsiPindah" class="form-control"required />
            </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Kabupaten/Kota Tujuan<sup class="text-danger">*</sup></label>
              <input type="text" id="kabupatenPindah" name="kabupatenPindah" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Kecamatan Tujuan<sup class="text-danger">*</sup></label>
              <input type="text" id="kecamatanPindah" name="kecamatanPindah" class="form-control" required/>
            </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Desa Tujuan<sup class="text-danger">*</sup></label>
              <input type="text" id="desaPindah" name="desaPindah" class="form-control" required/>
            </div>
            </div>


            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Dusun Tujuan<sup class="text-danger">*</sup></label>
              <input type="text" id="dusunPindah" name="dusunPindah" class="form-control" required/>
            </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Kodepos Tujuan</label>
              <input type="text" id="kodeposTujuan" name="kodeposTujuan" class="form-control" required/>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Telp Tujuan</label>
              <input type="text" id="telpTujuan" name="telpTujuan" class="form-control" required />
            </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Jenis Kepindahan<sup class="text-danger">*</sup> </label>
              <select name="jenisKepindahan" id="jenisKepindahan" class="form-control" required>
                <option value="">--Pilih--</option>
                <option value="Kepala Keluarga">Kepala Keluarga</option>
                <option value="Kep. Keluarga dan Seluruh Angg. Keluarga">Kep. Keluarga dan Seluruh Angg. Keluarga</option>
                <option value="Kep. Keluarga dan Sebagian Angg. Keluarga">Kep. Keluarga dan Sebagian Angg. Keluarga</option>
                <option value="Anggota Keluarga">Anggota Keluarga</option>
              </select>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Status KK Tidak Pindah<sup class="text-danger">*</sup> </label>
              <select name="statusKKtdakPindah" id="statusKKtdakPindah" class="form-control" required>
              <option value="">--Pilih--</option>
                <option value="Numpang KK">Numpang KK</option>
                <option value="Membuat KK Baru">Membuat KK Baru</option>
                <option value="Tidak Ada Anggota Keluarga yang Ditinggal">Tidak Ada Anggota Keluarga yang Ditinggal</option>
                <option value="No KK Tetap">No KK Tetap</option>
              </select>
            </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-md-6">
            <div class="form-group">
              <label class="font-weight-bold">Status KK Pindah<sup class="text-danger">*</sup></label>
              <select name="statusKKPindah" id="statusKKPindah" class="form-control" required>
              <option value="">--Pilih--</option>
                <option value="Numpang KK">Numpang KK</option>
                <option value="Membuat KK Baru">Membuat KK Baru</option>
                <option value="Nama Kepala Keluarga dan Nomor KK Tetap">Nama Kepala Keluarga dan Nomor KK Tetap</option>
              </select>
            </div>
            </div>
          </div>

            <br>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
          <button type="submit" name="submit" id="submit" class="btn btn-success">Tambah Data</>
        </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!--MODAL DETAIL-->
  <div id="modal-detail" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content modal-lg">
        <form role="form" id="form-detail" method="post" action="inputMutasi.php">
          <div class="modal-header">
            <h4 class="modal-title">Detail Data Mutasi</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body modal-lg">
            <div id="data-detail"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!--MODAL SUKSES-->
  <div class="modal fade" id="modal_Sukses" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="form-sukses">SUKSES</h5>
        </div>
        <form>
          <center>
            <i class="far fa-check-circle fa-5x "></i>
          </center>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="script/scriptDataMutasi.js"></script>
  <script src="js/js/datepicker.js"></script>
  <script>
      $(function() {
        $('[data-toggle="datepicker"]').datepicker({
          autoHide: true,
          zIndex: 2048
        });
      });
    </script>

</body>

</html>