<br>
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>Alamat</th>
                      <th>JK</th>
                      <th>S.Dalam Keluarga</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>Alamat</th>
                      <th>JK</th>
                      <th>S.Dalam Keluarga</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
            <?php
                include"koneksi.php";
                $no = 1;
                $view = mysqli_query($mysqli, " select
                                                        id,
                                                        nama,
                                                        nik,
                                                        noKK,
                                                        alamat,
                                                        jenisKelamin,
                                                        statusdalamKeluarga
                                                  from
                                                  penduduk
                                                  where statusdalamKeluarga Like'%Kepala Keluarga%'");
                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
        <tr>
            <td>
                <?php echo $no++; ?>
            </td>
            <td>
                <?php echo $user_data['nama']; ?>
            </td>
            <td>
                <?php echo $user_data['nik']; ?>
            </td>
            <td>
                <?php echo $user_data['alamat']; ?>
            </td>
            <td>
                <?php echo $user_data['jenisKelamin']; ?>
            </td>
            <td>
                <?php echo $user_data['statusdalamKeluarga']; ?>
            </td>
            <td>
            <center>
                <button title="Detail" type="button" class="btn btn-primary btn-xs" id="btndetailPenduduk" data-id=<?php echo $user_data['id']?> ><i class="fas fa-info-circle"></i></button>
                <button title="Ubah" type="button" class="btn btn-success btn-xs" id="btneditPenduduk" data-id=<?php echo $user_data['id']?> ><i class="far fa-edit"></i></button>
            <center>
            </td>
        </tr>
        <?php
                }
        ?>
        </tbody>
    </table>

    <script src="js/demo/datatables-demo.js"></script>

<!--MODAL TAMPIL DATA-->
      <!-- memulai modal nya. pada id="$myModal" harus sama dengan data-target="#myModal" pada tombol di atas -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel">Data Penduduk</h4>
              <div align="right">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
            </div>
            <!-- memulai untuk konten dinamis -->
            <!-- lihat id="data_siswa", ini yang di pangging pada ajax di bawah -->
            <div class="modal-body" id="detaildataPenduduk">
            </div>
            <!-- selesai konten dinamis -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      	<script>
	// ini menyiapkan dokumen agar siap grak :)
	$(document).ready(function(){
		// yang bawah ini bekerja jika tombol lihat data (class="view_data") di klik
		$('.view_data').click(function(){
			// membuat variabel id, nilainya dari attribut id pada button
			// id="'.$row['id'].'" -> data id dari database ya sob, jadi dinamis nanti id nya
			var id = $(this).attr("id");
			
			// memulai ajax
			$.ajax({
				url: 'detailPendudukKK.php',	// set url -> ini file yang menyimpan query tampil detail data siswa
				method: 'post',		// method -> metodenya pakai post. Tahu kan post? gak tahu? browsing aja :)
				data: {id:id},		// nah ini datanya -> {id:id} = berarti menyimpan data post id yang nilainya dari = var id = $(this).attr("id");
				success:function(data){		// kode dibawah ini jalan kalau sukses
					$('#detaildataPenduduk').html(data);	// mengisi konten dari -> <div class="modal-body" id="data_siswa">
					$('#myModal').modal("show");	// menampilkan dialog modal nya
				}
			});
		});
    });
    
