<?php

include"koneksi.php";
$id = htmlspecialchars($_POST['id']);
$noMutasi = htmlspecialchars($_POST['noMutasi']);
$jenisMutasi = htmlspecialchars($_POST['jenisMutasi']);
$kategoriPindah = htmlspecialchars($_POST['kategoriPindah']);
$nomorKK = htmlspecialchars($_POST['nomorKK']);
$nik = htmlspecialchars($_POST['nik']);
$kepalaKeluarga = htmlspecialchars($_POST['kepalaKeluarga']);
$alamatAsal = htmlspecialchars($_POST['alamatAsal']);
$rtAsal = htmlspecialchars($_POST['rtAsal']);
$rwAsal = htmlspecialchars($_POST['rwAsal']);
$provinsi = htmlspecialchars($_POST['provinsi']);
$kabupaten = htmlspecialchars($_POST['kabupaten']);
$kecamatan = htmlspecialchars($_POST['kecamatan']);
$desaAsal = htmlspecialchars($_POST['desaAsal']);
$dusunAsal = htmlspecialchars($_POST['dusunAsal']);
$kodeposAsal = htmlspecialchars($_POST['kodeposAsal']);
$telpAsal = htmlspecialchars($_POST['telpAsal']);
$namaPemohon = htmlspecialchars($_POST['namaPemohon']);
$alasanPindah = htmlspecialchars($_POST['alasanPindah']);
$alasanMohon = htmlspecialchars($_POST['alasanMohon']);
$noKKTujuan = htmlspecialchars($_POST['noKKTujuan']);
$nikKKTujuan = htmlspecialchars($_POST['nikKKTujuan']);
$tanggalKedatangan = htmlspecialchars($_POST['tanggalKedatangan']);
$tanggalKedatangan = htmlspecialchars(date('Y-m-d', strtotime($tanggalKedatangan)));
$alamatPindah = htmlspecialchars($_POST['alamatPindah']);
$rtPindah = htmlspecialchars($_POST['rtPindah']);
$rwPindah = htmlspecialchars($_POST['rwPindah']);
$provinsiPindah = htmlspecialchars($_POST['provinsiPindah']);
$kabupatenPindah = htmlspecialchars($_POST['kabupatenPindah']);
$kecamatanPindah = htmlspecialchars($_POST['kecamatanPindah']);
$desaPindah = htmlspecialchars($_POST['desaPindah']);
$dusunPindah = htmlspecialchars($_POST['dusunPindah']);
$kodeposTujuan = htmlspecialchars($_POST['kodeposTujuan']);
$telpTujuan = htmlspecialchars($_POST['telpTujuan']);
$jenisKepindahan = htmlspecialchars($_POST['jenisKepindahan']);
$statusKKtdakPindah = htmlspecialchars($_POST['statusKKtdakPindah']);
$statusKKPindah = htmlspecialchars($_POST['statusKKPindah']);

if ($noMutasi == '') {
    $data['error']['edit_noMutasi'] = 'No mutasi tidak boleh kosong';
}

    if (empty($data['error'])) {
        $query = "UPDATE mutasi SET
                                                  noMutasi = '$noMutasi',
                                                  jenisMutasi ='$jenisMutasi',
                                                  kategoriPindah ='$kategoriPindah',
                                                  nik ='$nik',
                                                  nomorKK ='$nomorKK',
                                                  kepalaKeluarga ='$kepalaKeluarga',
                                                  alamatAsal ='$alamatAsal',
                                                  rtAsal ='$rtAsal',
                                                  rwAsal ='$rwAsal',
                                                  provinsi ='$provinsi',
                                                  kabupaten ='$kabupaten',
                                                  kecamatan  ='$kecamatan',
                                                  desaAsal ='$desaAsal',
                                                  dusunAsal  ='$dusunAsal',
                                                  kodeposAsal  ='$kodeposAsal',
                                                  telpAsal ='$telpAsal',
                                                  namaPemohon ='$namaPemohon',
                                                  alasanPindah ='$alasanPindah',
                                                  alasanMohon ='$alasanMohon',
                                                  noKKTujuan ='$kategoriPindah',
                                                  nikKKTujuan ='$nikKKTujuan',
                                                  tanggalKedatangan ='$tanggalKedatangan',
                                                  alamatPindah ='$alamatPindah',
                                                  rtPindah ='$rtPindah',
                                                  rwPindah ='$rwPindah',
                                                  provinsiPindah ='$provinsiPindah',
                                                  kabupatenPindah ='$kabupatenPindah',
                                                  kecamatanPindah ='$kecamatanPindah',
                                                  desaPindah ='$desaPindah',
                                                  dusunPindah ='$dusunPindah',
                                                  kodeposTujuan ='$kodeposTujuan',
                                                  telpTujuan ='$telpTujuan',
                                                  jenisKepindahan ='$jenisKepindahan',
                                                  statusKKtdakPindah='$statusKKtdakPindah',
                                                  statusKKPindah ='$statusKKPindah'
                                        WHERE id='$id'
                                        ";
        mysqli_query($mysqli, $query)
        or die("Gagal Perintah SQL".mysql_error());
        $data['hasil'] = 'sukses';
    } else {
        $data['hasil'] = 'gagal';
    }
        echo json_encode($data);
