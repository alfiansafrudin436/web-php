
<?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, " select * from ketmeninggal where noSurat='$_POST[noSurat]'");
                 $user_data = mysqli_fetch_array($view);
                 if (!$view) {
                     printf("Error: %s\n", mysqli_error($mysqli));
                     exit();
                 }

?>

<form role="form" id="form-edit" method="post" action="editketMeninggalQuery.php">

<div class="form-group">
  <label class="font-weight-bold">No Surat<sup class="text-danger">*</sup></label>
  <input type="text" name="noSurat" class="form-control" value="<?php echo $user_data['noSurat']; ?>" readonly required/>
  <p style="color:red" id="error_noSurat"></p>
</div>

<div class="form-group">
<label class="font-weight-bold">NIK Meninggal<sup class="text-danger">*</sup></label>
<input name="nikMeninggal"  class="form-control" value="<?php echo $user_data['nikMeninggal']; ?>"required>
</div>

<div class="form-group">
  <label class="font-weight-bold">Nama<sup class="text-danger">*</sup></label>
  <input type="text" name="nama" class="form-control" value="<?php echo $user_data['nama']; ?>"required/>
</div>
<div class="form-group">
  <label class="font-weight-bold">Status<sup class="text-danger">*</sup></label>
  <input type="text" name="status" class="form-control" value="<?php echo $user_data['status']; ?>"required/>
</div>
<div class="form-group">
  <label class="font-weight-bold">Jenis Kelamin<sup class="text-danger">*</sup></label>
  <input type="text" name="jenisKelamin"  class="form-control" value="<?php echo $user_data['jenisKelamin']; ?>"required/>
</div>
<div class="form-group">
  <label class="font-weight-bold">Tempat Lahir<sup class="text-danger">*</sup></label>
  <input name="tempatLahir" class="form-control" value="<?php echo $user_data['tempatLahir']; ?>"required>
</div>
<br />
<div class="form-group">
  <label class="font-weight-bold">Tanggal Lahir<sup class="text-danger">*</sup></label>
  <input type="date" name="tanggalLahir" class="form-control" value="<?php echo $user_data['tanggalLahir']; ?>"required>
</div>

<div class="form-group">
  <label class="font-weight-bold">Kewarganegaraan<sup class="text-danger">*</sup></label>
  <input type="text" name="wargaNegara"  class="form-control" value="<?php echo $user_data['wargaNegara']; ?>"required>
</div>

<div class="form-group">
  <label class="font-weight-bold">Agama</label>
  <select name="agama" class="form-control" required>
                        <option value="Islam" <?php if ($user_data['agama']=='Islam') {
    ; ?> selected <?php
}?> >Islam</option>
                        <option value="Kristen" <?php if ($user_data['agama']=='Kristen') {
        ; ?> selected <?php
    }?> >Kristen</option>
                        <option value="Katholik" <?php if ($user_data['agama']=='Khatolik') {
        ; ?> selected <?php
    }?> >Katholik</option>
                        <option value="Hindu" <?php if ($user_data['agama']=='Hindu') {
        ; ?> selected <?php
    }?> >Hindu</option>
                        <option value="Budha" <?php if ($user_data['agama']=='Budha') {
        ; ?> selected <?php
    }?> >Budha</option>
                        <option value="Khonghucu" <?php if ($user_data['agama']=='Khonghucu') {
        ; ?> selected <?php
    }?> >Khonghucu</option>
                      </select>
</div>

<div class="form-group">
  <label class="font-weight-bold">Pekerjaan</label>
  <select name="pekerjaan" class="form-control" required>
  <option value="Belum/Tidak Bekerja" <?php if ($user_data['pekerjaan']=='Belum/Tidak Bekerja') {
        ; ?> selected <?php
    }?>>Belum/Tidak Bekerja</option>
                        <option value="Mengurus Rumah Tangga" <?php if ($user_data['pekerjaan']=='Mengurus Rumah Tangga"') {
        ; ?> selected <?php
    }?>>Mengurus Rumah Tangga</option>
                        <option value="Pelajar/Mahasiswa" <?php if ($user_data['pekerjaan']=='Pelajar/Mahasiswa') {
        ; ?> selected <?php
    }?>>Pelajar/Mahasiswa</option>
                        <option value="Pensiunan" <?php if ($user_data['pekerjaan']=='Pensiunan') {
        ; ?> selected <?php
    }?>>Pensiunan</option>
                        <option value="PNS" <?php if ($user_data['pekerjaan']=='PNS') {
        ; ?> selected <?php
    }?>>PNS</option>
                        <option value="TNI" <?php if ($user_data['pekerjaan']=='TNI') {
        ; ?> selected <?php
    }?>>TNI</option>
                      </select>
                      </div> 

<div class="form-group">
  <label class="font-weight-bold">Alamat<sup class="text-danger">*</sup></label>
  <textarea type="text" name="tempatTinggal" class="form-control" required><?php echo $user_data['tempatTinggal']; ?></textarea>
</div>

<div class="form-group">
  <label class="font-weight-bold">Tanggal Meninggal<sup class="text-danger">*</sup></label>
  <input type="date" name="tanggalMeninggal"  class="form-control" value="<?php echo $user_data['tanggalMeninggal']; ?>" data-toggle="datepicker"required/>
</div>

<div class="form-group">
  <label class="font-weight-bold">Meninggal Di<sup class="text-danger">*</sup></label>
  <input type="text" name="meninggalDi" " class="form-control" value="<?php echo $user_data['meninggalDi']; ?>"required>
</div>

<div class="form-group">
  <label class="font-weight-bold">Pejabat Yang Mengetahui<sup class="text-danger">*</sup></label>
  <input name="pejabat" class="form-control" value="<?php echo $user_data['pejabat']; ?>"required>
</div>
<br>
</form>