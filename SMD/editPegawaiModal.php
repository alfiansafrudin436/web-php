
<?php
                include"koneksi.php";
                $view = mysqli_query($mysqli, " select * from pegawai where id='$_POST[id]'");
                $user_data = mysqli_fetch_array($view);


?>

<form role="form" id="form-edit" method="post" action="editPegawaiQuery.php">

<div class="form-group">
  <input type="hidden" name="id" class="form-control" value="<?php echo $user_data['id']?>"required/>
</div>

<div class="form-group">
  <label class="font-weight-bold">NIK<sup class="text-danger" readonly>*</sup></label>
  <input type="text" name="nik"  class="form-control" value="<?php echo $user_data['nik']?>"readonly/>
  <p style="color:red" id="error_edit_nik"></p>
</div>

<div class="form-group">
  <label class="font-weight-bold">Nama<sup class="text-danger">*</sup></label>
  <input type="text" name="nama"  class="form-control" value="<?php echo $user_data['nama']?>"required/>

</div>

<div class="form-group">
  <label class="font-weight-bold">Alamat<sup class="text-danger">*</sup></label>
  <textarea type="text" name="alamat" class="form-control" required><?php echo $user_data['alamat']?></textarea>
</div>

<div class="form-group">
  <label class="font-weight-bold">NIP</label>
  <input type="text" name="nip" class="form-control" value="<?php echo $user_data['nip']?>"readonly/>
</div>

<div class="form-group">
  <label class="font-weight-bold">Jabatan<sup class="text-danger">*</sup></label>
  <select name="jabatan" class="form-control"  readonly>
    <option value="">--Pilih--</option>
    <option value="Lurah" <?php if ($user_data['jabatan']=='Lurah') {
    ; ?> selected <?php
}?>>Lurah</option>
    <option value="Sekretaris Desa"<?php if ($user_data['jabatan']=='Sekretaris Desa') {
        ; ?> selected <?php
    }?>>Sekretaris Desa</option>
    <option value="Urusan Umum Aparatur Desa dan Aset"<?php if ($user_data['jabatan']=='Urusan Umum Aparatur Desa dan Aset') {
        ; ?> selected <?php
    }?>>Urusan Umum Aparatur Desa dan Aset</option>
    <option value="Urusan Perancangan dan Keuangan"<?php if ($user_data['jabatan']=='Urusan Perancangan dan Keuangan') {
        ; ?> selected <?php
    }?>>Urusan Perancangan dan Keuangan</option>
    <option value="Seksi Pemerintahan"<?php if ($user_data['jabatan']=='Seksi Pemerintahan') {
        ; ?> selected <?php
    }?>>Seksi Pemerintahan</option>
    <option value="Seksi Pembangunan dan Pemberdayaan"<?php if ($user_data['jabatan']=='Seksi Pembangunan dan Pemberdayaan') {
        ; ?> selected <?php
    }?>>Seksi Pembangunan dan Pemberdayaan</option>
    <option value="Seksi Kemasyarakatan"<?php if ($user_data['jabatan']=='Seksi Kemasyarakatan') {
        ; ?> selected <?php
    }?>>Seksi Kemasyarakatan</option>
  </select>
</div>
<br>
</form>
