<br>
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No Surat</th>
                      <th>Nama Pemohon</th>
                      <th>JK</th>
                      <th>Alamat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>No Surat</th>
                      <th>Nama Pemohon</th>
                      <th>JK</th>
                      <th>Alamat</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
            <?php
                include"koneksi.php";
                $no = 1;
                $view = mysqli_query($mysqli, " select
                                                        noSurat,
                                                        nama,
                                                        jenisKelamin,
                                                        tempatTinggal
                                                  from
                                                  ketUmum order by noSurat");
                while ($user_data = mysqli_fetch_array($view)) {
                    ?>
        <tr>
            <td>
                <?php echo $no++; ?>
            </td>
            <td>
                <?php echo $user_data['noSurat']; ?>
            </td>
            <td>
                <?php echo $user_data['nama']; ?>
            </td>
            <td>
                <?php echo $user_data['jenisKelamin']; ?>
            </td>
            <td>
                <?php echo $user_data['tempatTinggal']; ?>
            </td>
            <td>
            <center>
            <?php
                    echo'
                            <a title="Cetak" type="button" class="btn btn-warning btn-xs" href="lapKetUmum.php?id='.$user_data['noSurat'].'"><i class="fas fa-print"></i></a>
                            <button title="Detail" type="button" class="btn btn-primary btn-xs" id="btndetail" data-id="'.$user_data['noSurat'].'"><i class="fas fa-info-circle"></i></button>
                            <button title="Ubah" type="button" class="btn btn-success btn-xs" id="btneditketUmum" data-id="'.$user_data['noSurat'].'" ><i class="far fa-edit"></i></button>
                            <button title="Hapus" type="button" class="btn btn-danger btn-xs" id="btnhapusketUmum" data-id="'.$user_data['noSurat'].'" ><i class="fas fa-trash"></i></button>

                    '; ?>
            <center>
            </td>
        </tr>
        <?php
                }
        ?>
        </tbody>
    </table>

    <script src="js/demo/datatables-demo.js"></script>

