
<?php
//memasukkan koneksi database
require_once("koneksi.php");

//jika berhasil/ada post['id'], jika tnikak ada ya tidak dijalankan
if ($_POST['noSurat']) {
    //membuat variabel id berisi post['id']
    $noSurat = $_POST['noSurat'];
    //query standart select where id
    $view = $mysqli->query("SELECT * FROM ketKelahiran WHERE noSurat='$noSurat'");
    //jika ada datanya
    if ($view->num_rows) {
        //fetch data ke dalam veriabel $user_data
        $user_data = $view->fetch_assoc();
        //menampilkan data dengan table
        echo '
		<p>Berikut ini adalah detail dari data surat keterangan kelahiran dengan nama <b>' . $user_data['namaAnak'] . '</b></p>
		<table class="table table-bordered">
			<tr>
				<th>NO SURAT</th>
				<td>' . $user_data['noSurat'] . '</td>
			</tr>
			<tr>
				<th>NAMA ANAK</th>
				<td>' . $user_data['namaAnak'] . '</td>
			</tr>
			<tr>
				<th>JENIS KELAMIN</th>
				<td>' . $user_data['jenisKelamin'] . '</td>
            </tr>
			<tr>
				<th>TEMPAT LAHIR</th>
				<td>' . $user_data['tempatLahir'] . '</td>
			</tr>
			<tr>
				<th>TANGGAL LAHIR</th>
				<td>' . $user_data['tanggalLahir'] . '</td>
            </tr>
			<tr>
				<th>AGAMA</th>
				<td>' . $user_data['agama'] . '</td>
            </tr>
			<tr>
				<th>ALAMAT</th>
				<td>' . $user_data['alamat'] . '</td>
			</tr>            
			<tr>
				<th>ANAK KE - </th>
				<td>' . $user_data['anakKe'] . '</td>
			</tr>
			<tr>
				<th>NIK AYAH</th>
				<td>' . $user_data['nikAyah'] . '</td>
            </tr>
			<tr>
				<th>NAMA AYAH</th>
				<td>' . $user_data['namaAyah'] . '</td>
			</tr>            
			<tr>
				<th>AGAMA AYAH</th>
				<td>' . $user_data['agamaAyah'] . '</td>
			</tr>            
			<tr>
				<th>PEKERJAAN AYAH</th>
				<td>' . $user_data['pekerjaanAyah'] . '</td>
			</tr>            
			<tr>
				<th>JENIS KELAMIN</th>
				<td>' . $user_data['jenisKelaminAyah'] . '</td>
			</tr>
			<tr>
				<th>TEMPAT LAHIR AYAH</th>
				<td>' . $user_data['tempatLahirAyah'] . '</td>
			</tr>
			<tr>
				<th>TANGGAL LAHIR AYAH</th>
				<td>' . $user_data['tanggalLahirAyah'] . '</td>
			</tr>
			<tr>
				<th>KEWARGANEGARAAN</th>
				<td>' . $user_data['wargaNegaraAyah'] . '</td>
			</tr>
			<tr>
				<th>ALAMAT</th>
				<td>' . $user_data['tempatTinggalAyah'] . '</td>
			</tr>
			<tr>
				<th>NIK IBU</th>
				<td>' . $user_data['nikIbu'] . '</td>
			</tr>
			<tr>
				<th>NAMA IBU</th>
				<td>' . $user_data['namaIbu'] . '</td>
			</tr>
			<tr>
				<th>JENIS KELAMIN</th>
				<td>' . $user_data['jenisKelaminIbu'] . '</td>
			</tr>
			<th>TEMPAT LAHIR IBU</th>
			<td>' . $user_data['tempatLahirIbu'] . '</td>
		</tr>
		<tr>
			<th>TANGGAL LAHIR IBU</th>
			<td>' . $user_data['tanggalLahirIbu'] . '</td>
		</tr>
			<tr>
				<th>KEWARGANEGARAAN</th>
				<td>' . $user_data['wargaNegaraIbu'] . '</td>
			</tr>

			<tr>
				<th>AGAMA</th>
				<td>' . $user_data['agamaIbu'] . '</td>
			</tr>
			<tr>
				<th>PEKERJAAN</th>
				<td>' . $user_data['pekerjaanIbu'] . '</td>
			</tr>
			<tr>
				<th>ALAMAT</th>
				<td>' . $user_data['tempatTinggalIbu'] . '</td>
			</tr>
			<tr>
				<th>YANG MENGETAHUI</th>
				<td>' . $user_data['pejabat'] . '</td>
			</tr>
			<tr>
			
			</tr>

		</table>
		';
    }
}
?>