
<?php
//memasukkan koneksi database
require_once("koneksi.php");
 
//jika berhasil/ada post['id'], jika tnikak ada ya tidak dijalankan
if ($_POST['id']) {
    //membuat variabel id berisi post['id']
    $id = $_POST['id'];
    //query standart select where id
    $view = $mysqli->query("SELECT * FROM mutasi WHERE id='$id'");
    //jika ada datanya
    if ($view->num_rows) {
        //fetch data ke dalam veriabel $user_data
        $user_data = $view->fetch_assoc();
        //menampilkan data dengan table
        echo '
		<p>Berikut ini adalah detail dari data Mutasi dengan nik <b>'.$user_data['nik'].'</b></p>
		<table class="table table-bordered">
			<tr>
				<th> NOMOR SURAT</th>
				<td>'.$user_data['noMutasi'].'</td>
			</tr>
			<tr>
				<th>JENIS MUTASI</th>
				<td>'.$user_data['jenisMutasi'].'</td>
			</tr>
			<tr>
				<th>KATEGORI KEPINDAHAN</th>
				<td>'.$user_data['kategoriPindah'].'</td>
            </tr>
			<tr>
				<th>NIK PEMOHON</th>
				<td>'.$user_data['nik'].'</td>
            </tr>
			<tr>
				<th>NOMOR KK</th>
				<td>'.$user_data['nomorKK'].'</td>
            </tr>
			<tr>
				<th>NAMA KEPALA KELUARGA</th>
				<td>'.$user_data['kepalaKeluarga'].'</td>
			</tr>
			<tr>
				<th>ALAMAT ASAL</th>
				<td>'.$user_data['alamatAsal'].'</td>
			</tr>
			<tr>
				<th>RT ASAL</th>
				<td>'.$user_data['rtAsal'].'</td>
            </tr>
			<tr>
				<th>RW ASAL</th>
				<td>'.$user_data['rwAsal'].'</td>
			</tr>
			<tr>
				<th>PROVINSI</th>
				<td>'.$user_data['provinsi'].'</td>
			</tr>
			<tr>
				<th>KABUPATEN</th>
				<td>'.$user_data['kabupaten'].'</td>
			</tr>
			<tr>
				<th>KECAMATAN</th>
				<td>'.$user_data['kecamatan'].'</td>
			</tr>
			<tr>
				<th>DESA ASAL</th>
				<td>'.$user_data['desaAsal'].'</td>
			</tr>
			<tr>
				<th>DUSUN ASAL</th>
				<td>'.$user_data['dusunAsal'].'</td>
			</tr>
			<tr>
				<th>KODEPOS ASAL</th>
				<td>'.$user_data['kodeposAsal'].'</td>
			</tr>
			<tr>
				<th>TELP ASAL</th>
				<td>'.$user_data['telpAsal'].'</td>
            </tr>
            <tr>
				<th>NAMA PEMOHON</th>
				<td>'.$user_data['namaPemohon'].'</td>
            </tr>
            <tr>
				<th>ALASAN PINDAH</th>
				<td>'.$user_data['alasanPindah'].'</td>
            </tr>
            <tr>
				<th>ALASAN PERMOHONAN</th>
				<td>'.$user_data['alasanMohon'].'</td>
            </tr>
            <tr>
				<th>NO KK TUJUAN</th>
				<td>'.$user_data['noKKTujuan'].'</td>
            </tr>
            <tr>
				<th>NIK KK TUJUAN</th>
				<td>'.$user_data['nikKKTujuan'].'</td>
            </tr>
            <tr>
				<th>TANGGAL KEDATANGAN</th>
				<td>'.$user_data['tanggalKedatangan'].'</td>
            </tr>
            <tr>
				<th>ALAMAT PINDAH</th>
				<td>'.$user_data['alamatPindah'].'</td>
            </tr>
            <tr>
				<th>RT PINDAH</th>
				<td>'.$user_data['rtPindah'].'</td>
            </tr>
            <tr>
				<th>RW PINDAH</th>
				<td>'.$user_data['rwPindah'].'</td>
			</tr>
			
			<tr>
				<th>PROVINSI TUJUAN</th>
				<td>'.$user_data['provinsiPindah'].'</td>
			</tr>
			<tr>
				<th>KABUPATEN TUJUAN</th>
				<td>'.$user_data['kabupatenPindah'].'</td>
			</tr>
			<tr>
				<th>KECAMATAN TUJUAN</th>
				<td>'.$user_data['kecamatanPindah'].'</td>
			</tr>
			<tr>
				<th>DESA TUJUAN</th>
				<td>'.$user_data['desaPindah'].'</td>
			</tr>
			<tr>
				<th>DUSUN TUJUAN</th>
				<td>'.$user_data['dusunPindah'].'</td>
			</tr>
			<tr>
				<th>KODEPOS TUJUAN</th>
				<td>'.$user_data['kodeposTujuan'].'</td>
			</tr>

            <tr>
				<th>TELP TUJUAN</th>
				<td>'.$user_data['telpTujuan'].'</td>
            </tr>
            <tr>
				<th>JENIS KEPINDAHAN</th>
				<td>'.$user_data['jenisKepindahan'].'</td>
            </tr>
            <tr>
				<th>STATUS KK TIDAK PINDAH</th>
				<td>'.$user_data['statusKKtdakPindah'].'</td>
            </tr>
            <tr>
				<th>STATUS KK PINDAH</th>
				<td>'.$user_data['statusKKPindah'].'</td>
            </tr>
	

		</table>
		';
    }
}
?>